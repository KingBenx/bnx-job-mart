<?php
// If it's going to need the database, then it's 
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class Newsletter extends DatabaseObject {
    
    protected static $table_name = "newsletter";
    protected static $db_fields = array('id','email');
    protected static $db_fields_insert = array('email');
    
    public $id;
    public $email;
}
?>