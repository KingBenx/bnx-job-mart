<?php
    require_once('../includes/initialize.php');
    if(isset($_GET['job'])&&isset($_GET['applicant'])){
        $apply = new Apply();
        $apply->student_id = $_GET['applicant'];
        $apply->job_id = $_GET['job'];
        $apply->apply_date = date('Y-m-d');
        $apply->save();

        #send mail to employer
        $getJobEmployer = Job::find_by_id($apply->job_id);
        $getJobTitle = $getJobEmployer->title;
        $getEmployerDetails = Company::find_by_id($getJobEmployer->company_id);
        $employer_email = $getEmployerDetails->email;
        $employer_name = $getEmployerDetails->name;

        #get student Details
        $getStudentDetails = Student::find_by_id($apply->student_id);
        $student_name = $getStudentDetails->name;
        $student_email = $getStudentDetails-> email;
        $student_telephone = $getStudentDetails ->telephone;
        #message details
        $message=wordwrap('Hello '.$employer_name.', '.$student_name.' ,email: '.$student_email.' has applied for the '.$getJobTitle.' job',70);
        $to=$employer_email;
        $subject=$getJobTitle.' Applicant';

        $mail = new PHPMailer;
        $mail->isSendmail();
        $mail->setFrom('info@bnxjobmart.com', 'Bnx Job Mart');
        $mail->addReplyTo('info@bnxjobmart.com', 'Bnx Job Mart');
        $mail->addAddress($to, $employer_name);
        $mail->Subject = $subject;
        $name = "Name: $student_name<br><br>";
        $email = "Email: $student_email<br><br>";
        $phone = "Phone: $student_telephone<br><br>";
        $message = "Message: $message<br><br>";
        $referrer = $_SERVER['HTTP_REFERER'] ? '<br><br><br>This Enquiry was submitted from: ' . $_SERVER['HTTP_REFERER'] : '';
        $body = "$name $email $phone $message $referrer";
        $mail->msgHTML($body);
        $mail->AltBody = $body;
        $mail->addAttachment('../public/img/logo.png');
        if (!$mail->send()) {
            $session->message("Application Mailer Error: " . $mail->ErrorInfo);
        } else {
            $session->message("Application Sent Successfully");
        }
        redirect_to("../profile/applications");
    }
?>