<?php
    require_once ('../includes/initialize.php');
    if(!$session->is_logged_in()){ redirect_to("login"); }
?>
<?php
    if(empty($_GET['id'])){
        $session->message("No User ID was provided. ");
        redirect_to('../profile/enquiries');
    }

    $contact_us = ContactUs::find_by_id($_GET['id']);
    if($contact_us->delete()){
        $session->message("The Message from {$contact_us->name} was deleted.");
        redirect_to('../profile/enquiries');
    } else {
        $session->message("The Message from {$contact_us->name} was not deleted.");
        redirect_to('../profile/enquiries');
    }
?>
<?php if(isset($database)){ $database->close_connection(); }