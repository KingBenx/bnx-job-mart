<div class="sidebar-wrapper">
    <ul class="nav">
    <li<?php if($_GET['page']=='dashboard' || !isset($_GET['page']) ||$_GET['page']==''): ?> class="active"<?php endif; ?>>
            <a href="dashboard">
                <i class="material-icons">mood</i>
                <p>Welcome <?php $names = explode (' ', $user->name); echo $names[0]; ?></p>
            </a>
        </li>
        <?php if($user->role=='administrator'){ ?>
        <li <?php if($_GET['page']=='jobapplicants'): ?> class="active"<?php endif; ?>>
            <a href="jobapplicants">
                <i class="material-icons text-gray">subject</i>
                <p>Job Applicants</p>
            </a>
        </li>
        <li <?php if($_GET['page']=='accounts'): ?> class="active"<?php endif; ?>>
            <a href="accounts">
                <i class="material-icons text-gray">people</i>
                <p>Company Accounts</p>
            </a>
        </li>
        <li<?php if($_GET['page']=='students'): ?> class="active"<?php endif; ?>>
            <a href="students">
                <i class="material-icons text-gray">school</i>
                <p>Student Accounts</p>
            </a>
        </li>
        <li<?php if($_GET['page']=='administrators'): ?> class="active"<?php endif; ?>>
            <a href="administrators">
                <i class="material-icons text-gray">person_outline</i>
                <p>Administrators</p>
            </a>
        </li>
        <li <?php if($_GET['page']=='newsletters'): ?> class="active"<?php endif; ?>>
            <a href="newsletters">
                <i class="material-icons text-gray">list</i>
                <p>Newsletter Signups</p>
            </a>
        </li>
        <li <?php if($_GET['page']=='enquiries'): ?> class="active"<?php endif; ?>>
            <a href="enquiries">
                <i class="material-icons text-gray">email</i>
                <p>Enquiries</p>
            </a>
        </li>
        <?php }elseif($user->role == 'student'){ ?>
        <li <?php if($_GET['page']=='specialization'): ?> class="active"<?php endif; ?>>
            <a href="specialization">
                <i class="material-icons text-gray">assessment</i>
                <p>Specialization</p>
            </a>
        </li>
        <li <?php if($_GET['page']=='credentials'): ?> class="active"<?php endif; ?>>
            <a href="credentials">
                <i class="material-icons text-gray">description</i>
                <p>Credentials</p>
            </a>
        </li>
        <li <?php if($_GET['page']=='adds'): ?> class="active"<?php endif; ?>>
                <a href="adds">
                    <i class="material-icons text-gray">rss_feed</i>
                    <p>Jobs</p>
                </a>
            </li>
            <li <?php if($_GET['page']=='applications'): ?> class="active"<?php endif; ?>>
                <a href="applications">
                    <i class="material-icons text-gray">alarm</i>
                    <p>Application Status</p>
                </a>
            </li>
        <li <?php if($_GET['page']=='receivedoffers'): ?> class="active"<?php endif; ?>>
                <a href="receivedoffers">
                    <i class="material-icons text-gray">hourglass_empty</i>
                    <p>Received Offers</p>
                </a>
            </li>
        <?php }else { ?>
            <li <?php if($_GET['page']=='jobs'): ?> class="active"<?php endif; ?>>
                <a href="jobs">
                    <i class="material-icons text-gray">subject</i>
                    <p>Advertise Jobs</p>
                </a>
            </li>
            <li <?php if($_GET['page']=='applicants'): ?> class="active"<?php endif; ?>>
                <a href="applicants">
                    <i class="material-icons text-gray">alarm</i>
                    <p>Applicants</p>
                </a>
            </li>
            <li <?php if($_GET['page']=='candidates'): ?> class="active"<?php endif; ?>>
                <a href="candidates">
                    <i class="material-icons text-gray">info_outline</i>
                    <p>Potential Candidates</p>
                </a>
            </li>
            <li <?php if($_GET['page']=='sentoffers'): ?> class="active"<?php endif; ?>>
                <a href="sentoffers">
                    <i class="material-icons text-gray">hourglass_empty</i>
                    <p>Sent Offers</p>
                </a>
            </li>
        <?php } ?>
    </ul>
</div>