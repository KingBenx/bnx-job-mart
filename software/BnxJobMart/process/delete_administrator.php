<?php
    require_once ('../includes/initialize.php');
	// if(!$session->is_logged_in()){ redirect_to("login"); }
?>
<?php
	if(empty($_GET['id'])){
		$session->message("No Administrator ID was provided. ");
		redirect_to('../profile/administrators');
	}

	$administrator = Administrators::find_by_id($_GET['id']);
	if($administrator->destroy()){
		$session->message("The Administrator {$administrator->name} was deleted.");
		redirect_to('../profile/administrators');
	} else {
		$session->message("The Administrator {$administrator->name} could not be deleted.");
		redirect_to('../profile/administrators');
	}
?>
<?php if(isset($database)){ $database->close_connection(); }