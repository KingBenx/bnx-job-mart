<div class="content">
    <div class="container-fluid">
        <div class="row">
            <h4 class="text text-capitalize text-danger"><?php if(isset($message)) echo output_message($message); ?></h4>
            <img style="opacity:0.5;" src="../public/img/cover.jpg" class="img img-responsive" alt="">
        </div>
    </div>
</div>