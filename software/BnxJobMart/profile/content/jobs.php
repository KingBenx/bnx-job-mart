<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Company Jobs</h4>
                        <p class="category">Your advertised Jobs</p>
                        <strong class="text-info">
                            <?php if(output_message($message)){ echo output_message($message); } ?>
                        </strong>
                    </div>
                    <div class="card-content table-responsive table-bordered">
                        <table class="table">
                            <thead class="text-primary">
                                <th>Title</th>
                                <th>Info</th>
                                <th>Description</th>
                                <th>Application Deadline</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </thead>
                            <tbody>
                                <?php
                                    $job = Jobs::find_by_sql('select*from job where company_id = '.$user->id.' order by id desc') ;
                                    foreach ($job as $value):
                                ?>
                                    <tr>
                                        <td>
                                            <?php echo strtoupper($value->title); ?>
                                        </td>
                                        <td>
                                            <p>
                                                <b>Specialization : </b>
                                                <?php echo strtoupper($value->specialization_field); ?>
                                                <br/>
                                                <b>Vacancies : </b>
                                                <?php echo $value->vacancy_no; ?>
                                                <br/>
                                                <b>Category : </b>
                                                <?php echo $value->category; ?>
                                                <br/>
                                                <b>Salary : </b>
                                                <?php echo strtoupper($value->salary); ?>
                                            </p>
                                        </td>
                                        <td>
                                            <?php echo $value->description; ?>
                                        </td>
                                        <td>
                                            <?php $date=strtotime($value->application_deadline); echo date('D M Y',$date); ?>
                                        </td>
                                        <td>
                                            <a href="jobs/edit/<?php echo $value->id; ?>">
                                                <i class="material-icons">mode_edit</i>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="../process/delete_job.php?id=<?php echo $value->id; ?>">
                                                <i class="material-icons">delete_forever</i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <?php if(!isset($_GET['edit'])){ ?>
                <div class="card">
                    <div class="card-header" data-background-color="blue">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="title">Add Job</h4>
                                <p class="category">Update any information</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <form action="../process/create_job.php" enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Job Title</label>
                                        <input type="text" name="title" value="" class="form-control" required="required"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Field</label>
                                        <select name="specialization_field" class="form-control" id="" required="required">
                                            <option value="it">Information Technology</option>
                                            <option value="multimedia">Multimedia</option>
                                            <option value="architecture">Architecture</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Salary</label>
                                        <input type="text" name="salary" required="required" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Vacancy No</label>
                                        <input type="number" min="0" max="1000" name="vacancy_no" class="form-control" required="required"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Category</label>
                                        <select name="category" class="form-control" id="" required="required">
                                            <option value="Full Time">Full Time</option>
                                            <option value="Part Time">Part Time</option>
                                            <option value="Contract Based">Contract Based</option>
                                            <option value="Remote">Remote</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group label-floating">
                                    <label class="control-label">Job Description</label>
                                    <textarea name="description" class="form-control" required="required"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group label-floating">
                                    <label class="control-label">Application Deadline</label>
                                    <input type="date" placeholder="YYYY-MM-DD" name="application_deadline" class="form-control" required="required"/>
                                </div>
                            </div>
                            <input type="hidden" name="company_id" value="<?php echo $user->id; ?>"/>
                            <button type="submit" name="submit" class="btn btn-primary pull-right">Add Job</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <?php }else{ $getJob = Jobs::find_by_id($_GET['edit']);  ?>
                <div class="card">
                    <div class="card-header" data-background-color="orange">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="title">Edit Job</h4>
                                <p class="category">Update any information</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <form action="../process/create_job.php" enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Job Title</label>
                                        <input type="text" name="title" value="<?php echo $getJob->title; ?>" class="form-control" required="required"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Field</label>
                                        <select name="specialization_field" class="form-control" id="" required="required">
                                            <option>Select Field</option>
                                            <option value="it">Information Technology</option>
                                            <option value="multimedia">Multimedia</option>
                                            <option value="architecture">Architecture</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Salary</label>
                                        <input type="text" name="salary" required="required" value="<?php echo $getJob->salary ?>" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Vacancy No</label>
                                        <input type="number" min="0" max="1000" value="<?php echo $getJob->vacancy_no; ?>" name="vacancy_no" class="form-control"
                                            required="required"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Category</label>
                                        <select name="category" class="form-control" id="" required="required">
                                            <option value="<?php echo $getJob->category; ?>">
                                                <?php echo $getJob->category; ?>
                                            </option>
                                            <option value="Full Time">Full Time</option>
                                            <option value="Part Time">Part Time</option>
                                            <option value="Contract Based">Contract Based</option>
                                            <option value="Remote">Remote</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group label-floating">
                                    <label class="control-label">Job Description</label>
                                    <textarea name="description" class="form-control" required="required">
                                        <?php echo $getJob->description; ?>
                                    </textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group label-floating">
                                    <label class="control-label">Application Deadline</label>
                                    <input type="date" placeholder="YYYY-MM-DD" name="application_deadline" value="<?php echo $getJob->application_deadline; ?>"
                                        class="form-control" required="required"/>
                                </div>
                            </div>
                            <input type="hidden" name="identity" value="<?php echo $getJob->id; ?>"/>
                            <button type="submit" name="submit" class="btn btn-primary pull-right">Edit Job</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>