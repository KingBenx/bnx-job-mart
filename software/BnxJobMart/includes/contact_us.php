<?php
// If it's going to need the database, then it's
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class ContactUs extends DatabaseObject {

	protected static $table_name = "contact_us";
	protected static $db_fields = array('id', 'name', 'email', 'subject','message','date');
	protected static $db_fields_insert = array('name', 'email', 'subject','message','date');

	public $id;
	public $name;
	public $email;
	public $subject;
	public $message;
	public $date;
}
?>