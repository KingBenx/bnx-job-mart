<?php
    require_once ('../includes/initialize.php');
    if(!$session->is_logged_in()){ redirect_to("../public"); }
?>
<?php
    if(empty($_GET['id'])){
        $session->message("No Job ID was provided. ");
        redirect_to('../profile/jobs');
    }

    $job = Jobs::find_by_id($_GET['id']);
    if($job->delete()){
        $session->message("The Job {$job->title} was deleted.");
        redirect_to('../profile/jobs');
    } else {
        $session->message("The Job {$job->title} was not deleted.");
        redirect_to('../profile/jobs');
    }
?>
<?php if(isset($database)){ $database->close_connection(); }