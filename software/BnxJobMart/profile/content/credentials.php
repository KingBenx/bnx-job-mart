<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Your Credentials</h4>
                        <p class="category">Your current credentials</p>
                        <strong class="text-info"><?php if(output_message($message)){ echo output_message($message); } ?></strong>  
                    </div>
                    <div class="card-content table-responsive table-bordered">
                        <table class="table">
                            <thead class="text-primary">
                                <th>Category</th>
                                <th>Description</th>
                                <th>File</th>  
                                <th>Edit</th>
                                <th>Delete</th>
                            </thead>
                            <tbody>
                                <?php
                                    $credential = Credentials::find_by_sql('select*from credentials where student_id = '.$user->id);
                                    foreach ($credential as $value):
                                ?>
                                    <tr>
                                        <td>
                                            <?php echo $value->category; ?>
                                        </td>
                                        <td>
                                            <?php echo $value->description; ?>
                                        </td>
                                        <td>
                                        <a href="../uploads/<?php echo $value->file_uploaded_path(); ?>" download><i class="material-icons">file_download</i></a>
                                        </td>
                                        <td>
                                            <a href="credentials/edit/<?php echo $value->id; ?>">
                                                <i class="material-icons">mode_edit</i>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="../process/delete_credential.php?id=<?php echo $value->id; ?>">
                                                <i class="material-icons">delete_forever</i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <?php if(!isset($_GET['edit'])){ ?>
                <div class="card">
                    <div class="card-header" data-background-color="blue">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="title">Add Credential</h4>
                                <p class="category">Update any information</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <form action="../process/create_credential.php" enctype="multipart/form-data" method="post">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Select Credential Category</label>
                                        <select class="form-control" name="category" id="category" required="required">
                                            <option value="Curriculum Vitae">Curriculum Vitae</option>
                                            <option value="Performance Statement">Performance Statement</option>
                                            <option value="Certificate">Certificate</option>
                                            <option value="Transcript">Transcript</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Upload Credential</label>
                                        <input type="file" name="file_upload" value="" class="form-control" required="required"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Description</label>
                                        <textarea name="description" class="form-control" id="description" required="required"></textarea>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="student_id" value="<?php echo $user->id; ?>">
                            <button type="submit" name="submit" class="btn btn-primary pull-right">Add Credential</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <?php }else{ $getCredential = Credentials::find_by_id($_GET['edit']);  ?>
                <div class="card">
                    <div class="card-header" data-background-color="orange">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4 class="title">Edit Credential</h4>
                                <p class="category">Update any information</p>
                            </div>
                            <div class="col-sm-6">
                                <a class="btn btn-info pull-right" data-toggle="modal" data-target="#myModalCredential">Change File</a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <form action="../process/create_credential.php" enctype="multipart/form-data" method="post">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Select Credential Category</label>
                                        <select class="form-control" name="category" id="category" required="required">
                                        <option value="<?php echo $getCredential->category; ?>"><?php echo $getCredential->category; ?></option>
                                            <option value="Curriculum Vitae">Curriculum Vitae</option>
                                            <option value="Performance Statement">Performance Statement</option>
                                            <option value="Certificate">Certificate</option>
                                            <option value="Transcript">Transcript</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Description</label>
                                        <textarea name="description" class="form-control" id="description" required="required">
                                        <?php echo $getCredential->description; ?>
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="student_id" value="<?php echo $getCredential->student_id; ?>">
                            <input type="hidden" name="identity" value="<?php echo $getCredential->id; ?>">
                            <button type="submit" name="submit" class="btn btn-warning pull-right">Edit Credential</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <div id="myModalCredential" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                    &times;
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="../process/edit_credential_file.php" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                                    <legend>CHANGE CREDENTIAL</legend>
                                    <input type="hidden" name="identity" value="<?php echo $getCredential->id;?>" id="identity" />
                                    <div class="form-group">
                                        <label for="file_upload">Credential</label>
                                        <input class="form-control" type="file" name="file_upload" aria-describedby="sizing-addon2" required="required" />
                                    </div>
                                    <input type="hidden" name="previous_image" value="<?php echo $getCredential->file_uploaded_path(); ?>"/>
                                    <div class="form-group">
                                        <button class="btn btn-block btn-success" type="submit" name="change_file">
                                            CHANGE CREDENTIAL
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>