<?php
    require_once '../includes/initialize.php';

	if(isset($_POST['submit'])){
		$administrator = new Administrators();
		$administrator->id = $_POST['identity'];
		$administrator->name = $_POST['name'];
		$administrator->email = $_POST['email'];
		$administrator->role = "administrator";
		$administrator->telephone = $_POST['telephone'];
		$administrator->access = 'grant';

		if(isset($_POST['identity'])){
			$administrator->save();
			$session->message("Administrator $administrator->name Details Updated Successfully");
		} else {
			$session->message("Administrator $administrator->name Details unsuccessful");
		}
		redirect_to("../profile/adminprofile");
	}
?>