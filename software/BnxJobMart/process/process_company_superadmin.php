<?php
    require_once '../includes/initialize.php';

	if(isset($_POST['submit'])){
		$company = new Company();
		$company->id = $_POST['identity'];
        $company->name = $_POST['name'];
		$company->url = $_POST['url'];
		$company->telephone = $_POST['telephone'];
		$company->location = $_POST['location'];
		$company->email = $_POST['email'];
		$company->role = $_POST['role'];
		$company->company_description = $_POST['description'];
		$company->password = password_hash($_POST['password'],PASSWORD_BCRYPT,array('cost'=>12));
		$verifypassword = $_POST['verifypassword'];
		$company->access = $_POST['access'];
		$company->attach_file($_FILES['file_upload']);

		if(isset($_POST['identity'])){
			$company->save();
			$session->message("Company $company->name Details Updated Successfully");
		} else {
			$company->save();
			$session->message("Companyt $company->name Saved successfully");
		}
		redirect_to("../profile/accounts");
	}
?>