<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Specialization Profile</h4>
                        <p class="category">Your Current Specialization</p>
                        <strong class="text-info">
                            <?php if(output_message($message)){ echo output_message($message); } ?>
                        </strong>
                    </div>
                    <div class="card-content table-responsive table-bordered">
                        <table class="table">
                            <thead class="text-primary">
                                <th>Specialization</th>
                                <th>Level</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </thead>
                            <tbody>
                                <?php
                                    $specialization = Specialization::find_by_sql('select*from specialization where student_id = '.$user->id) ;
                                    foreach ($specialization as $value):
                                ?>
                                    <tr>
                                        <td>
                                            <?php echo strtoupper($value->specialization); ?>
                                        </td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow=" <?php echo $value->level; ?>" aria-valuemin="0"
                                                    aria-valuemax="100" style="width: <?php echo $value->level; ?>%">
                                                    <span class="sr-only">
                                                        <?php echo $value->level; ?>% Complete</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="specialization/edit/<?php echo $value->id; ?>">
                                                <i class="material-icons">mode_edit</i>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="../process/delete_specialization.php?id=<?php echo $value->id; ?>">
                                                <i class="material-icons">delete_forever</i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <?php if(!isset($_GET['edit'])){ ?>
                <div class="card">
                    <div class="card-header" data-background-color="blue">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="title">Add Specialization</h4>
                                <p class="category">Update any information</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <form action="../process/create_specialization.php" enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Specialization Name</label>
                                        <input type="text" name="specialization" value="" class="form-control" required="required">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Level</label>
                                        <input type="range" name="level" min="0" max="100" value="" class="form-control" required="required">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="student_id" value="<?php echo $user->id; ?>">
                            <button type="submit" name="submit" class="btn btn-primary pull-right">Add Specializaion</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <?php }else{ $getspecialization = Specialization::find_by_id($_GET['edit']);  ?>
                <div class="card">
                    <div class="card-header" data-background-color="orange">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="title">Edit Specialization</h4>
                                <p class="category">Update any information</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <form action="../process/create_specialization.php" enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Specialization Name</label>
                                        <input type="text" name="specialization" value="<?php echo $getspecialization->specialization; ?>" class="form-control" required="required">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Level</label>
                                        <input type="range" name="level" min="0" max="100" value="<?php echo $getspecialization->level; ?>" class="form-control"
                                            required="required">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="student_id" value="<?php echo $getspecialization->student_id; ?>">
                            <input type="hidden" name="identity" value="<?php echo $getspecialization->id; ?>">
                            <button type="submit" name="submit" class="btn btn-warning pull-right">Edit Specialization</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>