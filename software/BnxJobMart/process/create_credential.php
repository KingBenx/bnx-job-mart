<?php
    require_once '../includes/initialize.php';

	if(isset($_POST['submit'])){
		$credentials = new Credentials();
		$credentials->id = $_POST['identity'];
		$credentials->category = $_POST['category'];
		$credentials->student_id = $_POST['student_id'];
		$credentials->description = $_POST['description'];
		$credentials->attach_file($_FILES['file_upload']);

		if(empty($credentials->category) || empty($credentials->description) ){
			$session->message("Failed to add Credential. Empty fields or incorrect data type");
		} elseif(isset($_POST['identity'])){
			$credentials->save();
			$session->message("Credential $credentials->category Details Updated Successfully");
		} else {
			$credentials->save();
			$session->message("Credential $credentials->category Saved successfully");
		}
		redirect_to("../profile/credentials");
	}
?>