<?php
// If it's going to need the database, then it's
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class Jobs extends DatabaseObject {

	protected static $table_name = "job";
	protected static $db_fields = array('id', 'title','category','specialization_field','vacancy_no','company_id' , 'description', 'salary','application_deadline');
	protected static $db_fields_insert = array('company_id' ,'title','category','specialization_field','vacancy_no', 'description', 'salary','application_deadline');
    protected static $db_fields_update = array('title','category','specialization_field','vacancy_no', 'description', 'salary','application_deadline');
    
    public $id;
    public $company_id;
    public $title;
    public $specialization_field;
    public $vacancy_no;
    public $category;
    public $salary;
	public $description;
    public $application_deadline;
}
?>