<?php
    require_once ('../includes/initialize.php');
	// if(!$session->is_logged_in()){ redirect_to("login"); }
?>
<?php
	if(empty($_GET['id'])){
		$session->message("No Company ID was provided. ");
		redirect_to('../profile/accounts');
	}

	$company = Company::find_by_id($_GET['id']);
	if($company->destroy()){
		$session->message("The Company {$company->name} was deleted.");
		redirect_to('../profile/accounts');
	} else {
		$session->message("The Company {$company->name} could not be deleted.");
		redirect_to('../profile/accounts');
	}
?>
<?php if(isset($database)){ $database->close_connection(); }