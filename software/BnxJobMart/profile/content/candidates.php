<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Student Accounts</h4>
                        <p class="category">Current students registered</p>
                        <strong class="text-info"><?php if(output_message($message)){ echo output_message($message); } ?></strong>  
                    </div>
                    <div class="card-content table-responsive table-bordered">
                        <table class="table">
                            <thead class="text-primary">
                                <th>Student</th>
                                <th>Contacts</th>
                                <th>Category</th>
                                <th>View Profile</th>
                            </thead>
                            <tbody>
                                <?php
                                    $student = Student::find_by_sql('select*from student where access=\'grant\'');
                                    foreach ($student as $value):
                                ?>
                                    <tr>
                                        <td>
                                            <img style="max-width: 120px;" class="img img-thumbnail img-responsive" src="../uploads/<?php echo $value->image_path(); ?>"
                                                alt="">
                                            <br/>
                                            <p>
                                                <b>Name</b> :
                                                <?php echo $value->name;  ?>
                                            </p>
                                        </td>


                                        <td>
                                            <p>
                                                <b>Email</b> :
                                                <?php echo $value->email;  ?>
                                            </p>
                                            <p>
                                                <b>Telephone</b> :
                                                <?php echo $value->telephone;  ?>
                                            </p>
                                        </td>
                                        <td>
                                            <?php echo strtoupper($value->category); ?>
                                        </td>
                                        <td>
                                        <a href="info/student/<?php echo $value->id; ?>">
                                            <i class="material-icons">info</i> View Info
                                        </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>