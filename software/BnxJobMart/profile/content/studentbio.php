<?php if(isset($_GET['id'])): ?>
<?php $getStudent=Student::find_by_id($_GET['id']); if(isset($getStudent)): ?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header" data-background-color="purple">
                                <h4 class="title">
                                    <?php echo $getStudent->name; ?> Specialization</h4>
                                <p class="category">Students Specializations</p>
                            </div>
                            <div class="card-content table-responsive table-bordered">
                                <table class="table">
                                    <thead class="text-primary">
                                        <th>Specialization</th>
                                        <th>Level</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                    $specialization = Specialization::find_by_sql('select*from specialization where student_id = '.$getStudent->id) ;
                                    foreach ($specialization as $value):
                                ?>
                                            <tr>
                                                <td>
                                                    <?php echo strtoupper($value->specialization); ?>
                                                </td>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow=" <?php echo $value->level; ?>" aria-valuemin="0"
                                                            aria-valuemax="100" style="width: <?php echo $value->level; ?>%">
                                                            <span class="sr-only">
                                                                <?php echo $value->level; ?>% Complete</span>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header" data-background-color="blue">
                                <h4 class="title">
                                    <?php echo $getStudent->name; ?> Credentials</h4>
                                <p class="category">
                                    <?php echo $getStudent->name; ?> current credentials</p>
                                <strong class="text-info">
                                    <?php if(output_message($message)){ echo output_message($message); } ?>
                                </strong>
                            </div>
                            <div class="card-content table-responsive table-bordered">
                                <table class="table">
                                    <thead class="text-primary">
                                        <th>Category</th>
                                        <th>Description</th>
                                        <th>File</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                    $credential = Credentials::find_by_sql('select*from credentials where student_id = '.$getStudent->id);
                                    foreach ($credential as $value):
                                ?>
                                            <tr>
                                                <td>
                                                    <?php echo $value->category; ?>
                                                </td>
                                                <td>
                                                    <?php echo $value->description; ?>
                                                </td>
                                                <td>
                                                    <a href="../uploads/<?php echo $value->file_uploaded_path(); ?>" download>
                                                        <i class="material-icons">file_download</i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div class="col-md-4">
                <div class="card card-profile">
                    <div class="card-avatar">
                        <img style="width:120px;height:120px;overflow:hidden;" class="img" src="../uploads/<?php echo $getStudent->image_path(); ?>"
                        />
                    </div>
                    <div class="content">
                        <h6 class="category text-gray">POTENTIAL CANDIDATE</h6>
                        <h4 class="card-title">
                            <?php echo $getStudent->name; ?>
                        </h4>
                        <p class="card-content">
                            <div class="row">
                                <div class="col-sm-6">
                                    <b>Email</b>
                                </div>
                                <div class="col-sm-6">
                                    <?php echo $getStudent->email; ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <b>Telephone</b>
                                </div>
                                <div class="col-sm-6">
                                    <?php echo $getStudent->telephone; ?>
                                </div>
                            </div>
                        </p>
                        <?php if($user->role=='company'): ?>
                        <a data-toggle="modal" data-target="#myModalOffer" class="btn btn-primary btn-round">Make Offer</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModalOffer" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <form action="../process/process_offer.php" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                    <legend>Make Offer</legend>
                    <div class="form-group">
                        <label for="title">Job Title</label>
                        <input type="text" class="form-control" name="title" required="required"/>
                    </div>
                    <div class="form-group">
                        <label for="salary">Salary</label>
                        <input type="text" class="form-control" name="salary" required="required"/>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Work Category</label>
                        <select name="category" class="form-control" id="" required="required">
                            <option value="Full Time">Full Time</option>
                            <option value="Part Time">Part Time</option>
                            <option value="Contract Based">Contract Based</option>
                            <option value="Remote">Remote</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" class="form-control" required="required"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="date">Offer Deadline</label>
                        <input type="date" class="form-control" placeholder="YYYY-MM-DD" name="offer_deadline" required="required"/>
                    </div>
                    <input type="hidden" name="company_id" value="<?php echo $user->id; ?>">
                    <input type="hidden" name="student_id" value="<?php echo $getStudent->id; ?>">
                    <input type="hidden" name="status" value="decline">
                    <div class="form-group">
                        <button class="btn btn-block btn-success" type="submit" name="submit">
                            MAKE OFFER
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<?php endif; ?>