<?php
    require_once('../includes/initialize.php');
    if(isset($_POST['submit'])){
        $specialization = new Specialization();
        $specialization->id = $_POST['identity'];
        $specialization->specialization = $_POST['specialization'];
        $specialization->student_id = $_POST['student_id'];
        $specialization->level = $_POST['level'];
        
        if(empty($specialization->specialization) || empty($specialization->level) ){
			$session->message("Please fill in all the fields");
		} elseif (isset($_POST['identity'])){
			$specialization->save();
			$session->message("Updated Specialization $specialization->specialization successfully");
		} else {
			$specialization->save();
			$session->message("Saved Specialization $specialization->specialization successfully");
		}
        redirect_to("../profile/specialization");
    }
?>