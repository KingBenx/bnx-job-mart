<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Received Company Offers</h4>
                        <p class="category">Current offers received</p>
                        <strong class="text-info">
                            <?php if(output_message($message)){ echo output_message($message); } ?>
                        </strong>
                    </div>
                    <div class="card-content table-responsive table-bordered">
                        <table class="table">
                            <thead class="text-primary">
                                <th>Company</th>
                                <th>Title</th>
                                <th>Salary</th>
                                <th>Description</th>
                                <th>Deadline</th>
                                <th>Your Decision</th>
                                <th>Reply</th>
                                <th>Delete</th>
                            </thead>
                            <tbody>
                                <?php
                                    $offer = Offers::find_by_sql('select*from offer where student_id= '.$user->id.' order by status asc');
                                    foreach ($offer as $value):
                                ?>
                                    <tr>

                                        <td>
                                            <div class="row">
                                                <img style="max-width: 120px;" class="img img-thumbnail img-responsive" src="../uploads/<?php $getCompany=Company::find_by_id($value->company_id); echo $getCompany->image_path(); ?>"
                                                    alt="">
                                            </div>
                                            <div class="row">
                                                <b>
                                                    <small>
                                                        <?php  $getCompany=Company::find_by_id($value->company_id); echo $getCompany->name; ?>
                                                    </small>
                                                </b>
                                            </div>
                                        </td>
                                        <td>
                                            <?php echo $value->title; ?>
                                        </td>
                                        <td>
                                            <?php echo $value->salary; ?>
                                        </td>
                                        <td>
                                            <?php echo $value->description; ?>
                                        </td>
                                        <td>
                                            <?php $date=strtotime($value->offer_deadline); echo date('D M Y',$date); ?>
                                        </td>
                                        <td>
                                            <?php echo $value->status; ?>
                                        </td>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#myModalOffer<?php echo $value->id; ?>">
                                                <i class="material-icons">mode_edit</i>
                                            </a>
                                        </td>
                                        <div id="myModalOffer<?php echo $value->id; ?>" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">
                                                            &times;
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form action="../process/respond_to_offer.php" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                                                            <legend><?php  $getCompany=Company::find_by_id($value->company_id); echo strtoupper($getCompany->name); ?> OFFER
                                                            </legend>
                                                            <div class="form-group">
                                                                <label for="title">Job Title</label>
                                                                <input type="text" class="form-control" name="title" value="<?php echo $value->title; ?>" required="required" readonly/>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="salary">Salary</label>
                                                                <input type="text" class="form-control" name="salary" value="<?php echo $value->salary; ?>" required="required" readonly />
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="description">Description</label>
                                                                <textarea name="description" class="form-control" required="required" readonly>
                                                                    <?php echo $value->description; ?>
                                                                </textarea>
                                                            </div>
                                                            <div class="form-group label-floating">
                                                                <label class="control-label">Your Decision</label>
                                                                <select name="status" class="form-control" id="" required="required">
                                                                    <option value="<?php echo $value->status; ?>">
                                                                        <?php echo $value->status; ?>
                                                                    </option>
                                                                    <option value="accept">accept</option>
                                                                    <option value="decline">decline</option>
                                                                </select>
                                                            </div>
                                                            <input type="hidden" name="offer_deadline" value="<?php echo $value->offer_deadline; ?>">
                                                            <input type="hidden" name="category" value="<?php echo $value->category; ?>" />
                                                            <input type="hidden" name="identity" value="<?php echo $value->id; ?>" />
                                                            <div class="form-group">
                                                                <button class="btn btn-block btn-success" type="submit" name="submit">
                                                                    UPDATE OFFER
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <td>
                                            <a href="../process/delete_received_offer.php?id=<?php echo $value->id; ?>">
                                                <i class="material-icons">delete_forever</i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>