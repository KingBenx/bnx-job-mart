<?php
    require_once '../includes/initialize.php';

	if(isset($_POST['submit'])){
		$offer = new Offers();
		$offer->id = $_POST['identity'];
        $offer->company_id = $_POST['company_id'];
        $offer->title = $_POST['title'];
		$offer->student_id = $_POST['student_id'];
        $offer->salary = $_POST['salary'];
        $offer->description = $_POST['description'];
        $offer->offer_deadline = $_POST['offer_deadline'];
        $offer->category = $_POST['category'];
        $offer->status = $_POST['status'];

		if(empty($offer->category) || empty($offer->salary)|| empty ($offer->description)|| empty($offer->offer_deadline) ){
			$session->message("Failed to make offer. Empty fields or incorrect data type");
		} elseif(isset($_POST['identity'])){
			$offer->save();
			$session->message("Offer Details Updated Successfully");
		} else {
			$offer->save();
			$session->message("Offer Saved successfully");
		}

		#get Employer and student details
		$getStudentDetails = Student::find_by_id($offer->student_id);
		$getEmployerDetails = Company:: find_by_id($offer->company_id);

		$message=wordwrap('Hello '.$getEmployerDetails->name.' , '.$getStudentDetails->name.' has responded to yout offer! Check our site for more information',70);
        $to=$getEmployerDetails->email;
        $subject=$offer->title.' Job Offer Response';
        $mail = new PHPMailer;
        $mail->isSendmail();
        $mail->setFrom('info@bnxjobmart.com', 'Bnx Job Mart');
        $mail->addReplyTo('info@bnxjobmart.com', 'Bnx Job Mart');
        $mail->addAddress($to, $getEmployerDetails->name);
        $mail->Subject = $subject;
        $name = "Name: $getStudentrDetails->name<br><br>";
        $email = "Email: $getStudentDetails->email<br><br>";
        $phone = "Phone: $gettudentDetails->telephone<br><br>";
        $message = "Message: $message<br><br>";
        $referrer = $_SERVER['HTTP_REFERER'] ? '<br><br><br>This message was submitted from: ' . $_SERVER['HTTP_REFERER'] : '';
        $body = "$name $email $phone $message $referrer";
        $mail->msgHTML($body);
        $mail->AltBody = $body;
        $mail->addAttachment('../public/img/logo.png');
		$mail->send();
		
		redirect_to("../profile/receivedoffers");
	}
?>