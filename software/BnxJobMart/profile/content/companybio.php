<?php if(isset($_GET['id'])): ?>
<?php $getCompany=Company::find_by_id($_GET['id']); if(isset($getCompany)): ?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header" data-background-color="purple">
                                <h4 class="title">
                                    <?php echo $getCompany->name; ?> Advertised Jobs</h4>
                                <p class="category">Jobs advertised by
                                    <?php echo $getCompany->name; ?>
                                </p>
                            </div>
                            <div class="card-content table-responsive table-bordered">
                                <table class="table">
                                    <thead class="text-primary">
                                        <th>Job</th>
                                        <th>Desc</th>
                                        <th>Field</th>
                                        <th>Vacancy No</th>
                                        <th>Work Category</th>
                                        <th>Deadline</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                    $jobs = Jobs::find_by_sql('select*from job where company_id = '.$getCompany->id.' order by application_deadline asc') ;
                                    foreach ($jobs as $value):
                                ?>
                                            <tr>
                                                <td>
                                                    <?php echo strtoupper($value->title); ?>
                                                </td>
                                                <td>
                                                    <?php echo $value->description; ?>
                                                </td>
                                                <td>
                                                    <?php echo strtoupper($value->specialization_field); ?>
                                                </td>
                                                <td>
                                                    <?php echo $value->vacancy_no; ?>
                                                </td>
                                                <td>
                                                    <?php echo $value->category; ?>
                                                </td>
                                                <td>
                                                    <?php $date=strtotime($value->application_deadline); echo date('D M Y',$date); ?>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header" data-background-color="blue">
                                <h4 class="title">
                                    <?php echo $getCompany->name; ?> Job Offers</h4>
                                <p class="category">
                                    <?php echo $getCompany->name; ?> current job offers</p>
                                <strong class="text-info">
                                    <?php if(output_message($message)){ echo output_message($message); } ?>
                                </strong>
                            </div>
                            <div class="card-content table-responsive table-bordered">
                                <table class="table">
                                    <thead class="text-primary">
                                        <th>Student</th>
                                        <th>Category</th>
                                        <th>Job</th>
                                        <th>Description</th>
                                        <th>Salary</th>
                                        <th>Response Deadline</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                    $offers = Offers::find_by_sql('select*from offer where company_id = '.$getCompany->id.' order by id desc');
                                    foreach ($offers as $value):
                                ?>
                                            <tr>
                                            <td>
                                            <div class="row">
                                                <img style="max-width: 120px;" class="img img-thumbnail img-responsive" src="../uploads/<?php $getStudent=Student::find_by_id($value->student_id); echo $getStudent->image_path(); ?>"
                                                    alt="">
                                            </div>
                                            <div class="row">
                                                <b>
                                                    <small>
                                                        <?php $getStudent=Student::find_by_id($value->student_id);  echo $getStudent->name; ?>
                                                    </small>
                                                </b>
                                            </div>
                                        </td>
                                                <td>
                                                    <?php echo strtoupper($value->category); ?>
                                                </td>
                                                <td>
                                                    <?php echo $value->title; ?>
                                                </td>
                                                <td>
                                                    <?php echo $value->description; ?>
                                                </td>
                                                <td>
                                                    <?php echo strtoupper($value->salary); ?>
                                                </td>
                                                <td>
                                                    <?php $date=strtotime($value->offer_deadline); echo date('D M Y',$date); ?>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div class="col-md-4">
                <div class="card card-profile">
                    <div class="card-avatar">
                        <img style="width:120px;height:120px;overflow:hidden;" class="img" src="../uploads/<?php echo $getCompany->image_path(); ?>"
                        />
                    </div>
                    <div class="content">
                        <h6 class="category text-gray">POTENTIAL EMPLOYER</h6>
                        <h4 class="card-title">
                            <?php echo $getCompany->name; ?>
                        </h4>
                        <p class="card-content">
                            <div class="row">
                                <div class="col-sm-6">
                                    <b>Email</b>
                                </div>
                                <div class="col-sm-6 text-center">
                                <small>
                                    <a href="<?php echo $getCompany->email ?>"><?php echo $getCompany->email; ?></a>
                                </small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <b>Telephone</b>
                                </div>
                                <div class="col-sm-6 text-center">
                                    <?php echo $getCompany->telephone; ?>
                                </div>
                            </div>
                        </p>
                        <a href="<?php echo $getCompany->url; ?>" class="btn btn-primary btn-round">Visit Site</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<?php endif; ?>