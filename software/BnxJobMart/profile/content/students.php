<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div <?php if(!isset($_GET[ 'edit'])){ ?>class="col-md-12"
                <?php }else{?>class="col-md-8"
                <?php } ?>>
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Student Accounts</h4>
                        <p class="category">Current students registered</p>
                        <strong class="text-info"><?php if(output_message($message)){ echo output_message($message); } ?></strong>  
                    </div>
                    <div class="card-content table-responsive table-bordered">
                        <table class="table">
                            <thead class="text-primary">
                                <th>Student</th>
                                <th>Contacts</th>
                                <th>Category</th>
                                <th>Access</th>
                                <th>View Profile</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </thead>
                            <tbody>
                                <?php
                                    $student = Student::find_all_order_by_field_asc('access');
                                    foreach ($student as $value):
                                ?>
                                    <tr>
                                        <td>
                                            <img style="max-width: 120px;" class="img img-thumbnail img-responsive" src="../uploads/<?php echo $value->image_path(); ?>"
                                                alt="">
                                            <br/>
                                            <p>
                                                <b>Name</b> :
                                                <?php echo $value->name;  ?>
                                            </p>
                                            <p>
                                                <b>Student No</b> :
                                                <?php echo $value->student_no;  ?>
                                            </p>
                                        </td>


                                        <td>
                                            <p>
                                                <b>Email</b> :
                                                <?php echo $value->email;  ?>
                                            </p>
                                            <p>
                                                <b>Telephone</b> :
                                                <?php echo $value->telephone;  ?>
                                            </p>
                                        </td>
                                        <td>
                                            <?php echo strtoupper($value->category); ?>
                                        </td>
                                        <td>
                                            <?php echo $value->access; ?>
                                        </td>
                                        <td>
                                        <a href="info/student/<?php echo $value->id; ?>">
                                            <i class="material-icons">info</i> View Info
                                        </a>
                                        </td>
                                        <td>
                                            <a href="students/edit/<?php echo $value->id; ?>">
                                                <i class="material-icons">mode_edit</i>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="../process/delete_student.php?id=<?php echo $value->id; ?>">
                                                <i class="material-icons">delete_forever</i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div <?php if(isset($_GET[ 'edit'])): ?>class="col-md-4"
                <?php endif; ?>>
                <?php  if(isset($_GET['edit'])){ $studentdetails = Student::find_by_id($_GET['edit']); ?>
                <div class="card">
                    <div class="card-header" data-background-color="orange">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4 class="title">Edit Student</h4>
                                <p class="category">Update any information</p>
                            </div>
                            <div class="col-sm-6">
                                <img class="img img-thumbnail img-responsive pull-right" style="max-width: 150px;" src="../uploads/<?php echo $studentdetails->image_path();?>">
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <form action="../process/process_student_superadmin.php" enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Student Name</label>
                                        <input type="text" class="form-control" name="name" id="name" value="<?php echo $studentdetails->name; ?>" required="required">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Student Number</label>
                                        <input type="text" class="form-control" name="student_no" id="student_no" value="<?php echo $studentdetails->student_no; ?>"
                                            required="required">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Email</label>
                                        <input type="email" class="form-control" name="email" id="email" value="<?php echo $studentdetails->email; ?>" required="required">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Telephone</label>
                                        <input type="tel" class="form-control" name="telephone" id="telephone" value="<?php echo $studentdetails->telephone; ?>"
                                            required="required">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="control-label">Category</label>
                                    <select class="form-control" id="category" name="category" required="required">
                                        <option value="it">Information Technology</option>
                                        <option value="multimedia">Multimedia</option>
                                        <option value="architecture">Architecture</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Access</label>
                                        <select class="form-control" name="access" id="access" required="required">
                                            <option value="<?php echo $studentdetails->access; ?>">
                                                <?php echo $studentdetails->access; ?>
                                            </option>
                                            <option value="deny">deny</option>
                                            <option value="grant">grant</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="identity" value="<?php echo $studentdetails->id; ?>">
                            <button type="submit" name="submit" class="btn btn-warning pull-right">Update Student Profile</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <?php  } ?>
            </div>
        </div>
    </div>
</div>