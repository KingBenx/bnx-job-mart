<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Received Enquiries</h4>
                        <p class="category">Received message from online users</p>
                        <strong class="text-info"><?php if(output_message($message)){ echo output_message($message); } ?></strong>  
                    </div>
                    <div class="card-content table-responsive table-bordered">
                        <table class="table">
                            <thead class="text-primary">
                                <th>Date</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Subject</th>
                                <th>Message</th>
                                <th>Reply</th>
                                <th>Delete</th>
                            </thead>
                            <tbody>
                                <?php
                                    $contact_us = ContactUs::find_by_sql('select*from contact_us order by date desc');
                                    foreach ($contact_us as $value):
                                ?>
                                    <tr>
                                        <td>
                                            <?php $date=strtotime($value->date); echo date('D M Y',$date); ?>
                                        </td>
                                        <td>
                                            <?php echo $value->name; ?>
                                        </td>
                                        <td>
                                            <?php echo $value->email; ?>
                                        </td>
                                        <td>
                                            <?php echo $value->subject; ?>
                                        </td>
                                        <td>
                                            <?php echo $value->message; ?>
                                        </td>
                                        <td>
                                            <a href="mailto:<?php echo $value->email; ?>">
                                                <i class="material-icons">reply</i>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="../process/delete_message.php?id=<?php echo $value->id; ?>">
                                                <i class="material-icons">delete_forever</i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>