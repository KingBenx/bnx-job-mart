<?php
    require_once '../includes/initialize.php';

    $user_profile=new Administrators();
    $user_profile->id=$_POST['identity'];
    $user_profile->password=password_hash($_POST['password'],PASSWORD_BCRYPT,array('cost'=>12));
    $oldpass = Administrators::find_by_id($_POST['identity']);

    if(!password_verify($_POST['confirmpassword'],$user_profile->password)){
        $session->message("Password fields don't match");
        redirect_to("../profile/adminprofile");
    } elseif(!password_verify($_POST['oldpassword'],$oldpass->password)){
        $session->message("Old password doesn't match");
        redirect_to("../profile/adminprofile");
    }else{
        $user_profile->update_password();
        $session->message("Password has been successfully updated");
        redirect_to("../profile/adminprofile");
    }

?>