<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Application Status</h4>
                        <p class="category">Current Applicants</p>
                        <strong class="text-info">
                            <?php if(output_message($message)){ echo output_message($message); } ?>
                        </strong>
                    </div>
                    <div class="card-content table-responsive table-bordered">
                        <table class="table">
                            <thead class="text-primary">
                                <th>Job Title</th>
                                <th>Student</th>
                                <th>Job Description</th>
                                <th>Application Date</th>
                                <th>Interview Date</th>
                                <th>View Student</th>
                                <th>Set Appointment</th>
                                <th>Delete</th>
                            </thead>
                            <tbody>
                                <?php
                                    $getJobId=Jobs::find_by_sql('select*from job where company_id = '.$user->id);
                                    foreach($getJobId as $value):
                                    $apply = Apply::find_by_sql('select*from job_applicants where job_id = '.$value->company_id.' order by id desc') ;
                                    foreach ($apply as $value2):
                                ?>
                                    <tr>
                                        <td>
                                            <?php $getJob=Jobs::find_by_id($value2->job_id); echo $getJob->title; ?>
                                        </td>
                                        <td>
                                            <div class="row">
                                                <img style="max-width: 120px;" class="img img-thumbnail img-responsive" src="../uploads/<?php  $getStudent=Student::find_by_id($value2->student_id); echo $getStudent->image_path(); ?>"
                                                    alt="">
                                            </div>
                                            <div class="row">
                                                <b>
                                                    <small>
                                                        <?php $getStudent=Student::find_by_id($value2->student_id); echo $getStudent->name; ?>
                                                    </small>
                                                </b>
                                            </div>
                                        </td>
                                        <td>
                                            <?php  $getJob=Jobs::find_by_id($value2->job_id); echo $getJob->description; ?>
                                        </td>
                                        <td>
                                            <?php $date=strtotime($value2->apply_date); echo date('D M Y',$date); ?>
                                        </td>
                                        <td>
                                            <?php if($value2->interview_date == NULL){
                                                echo "PENDING";
                                            }else{
                                                $date=strtotime($value2->interview_date); echo date('D M Y',$date); 
                                            }
                                             ?>
                                        </td>
                                        <td class="text-center">
                                            <a href="../profile/info/student/<?php echo $value2->student_id; ?>">
                                                <i class="material-icons">visibility</i>
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a href="#" data-toggle="modal" data-target="#myModalStudent<?php echo $value2->id; ?>">
                                                <i class="material-icons"><i class="material-icons">help_outline</i></i>
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a href="">
                                                <i class="material-icons">delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                    <div id="myModalStudent<?php echo $value2->id; ?>" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">
                                                        &times;
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="../process/set_interview.php" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                                                        <legend>Set
                                                            <?php $getStudent=Student::find_by_id($value2->student_id); echo $getStudent->name; ?>'s Interview Date</legend>
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Interview Date</label>
                                                            <input type="date" name="interview_date" placeholder="YYYY-MM-DD" class="form-control" required="required" />
                                                        </div>
                                                        <input type="hidden" name="identity" value="<?php echo $value2->id; ?>"/>
                                                        <button name="submit" type="submit" class="btn btn-block btn-success">SET INTERVIEW DATW</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                    <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>