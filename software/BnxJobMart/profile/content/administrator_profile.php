<?php if($user->role=='administrator'): ?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Edit Profile</h4>
                        <p class="category">Complete your profile</p>
                        <p class="text-danger"><?php if(isset($message)){ echo output_message($message); } ?></p>
                    </div>
                    <div class="card-content">
                        <form action="../process/edit_admin_profile.php" enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Name</label>
                                        <input type="text" name="name" value="<?php echo $user->name; ?>" class="form-control" required="required" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Email</label>
                                        <input type="email" name="email" value="<?php echo $user->email; ?>" class="form-control" required="required" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Telephone</label>
                                        <input type="tel" name="telephone" value="<?php echo $user->telephone; ?>" class="form-control" required="required" />
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="identity" value="<?php echo $user->id; ?>" />
                            <a href="#" data-toggle="modal" data-target="#myModalPassword" class="btn btn-primary btn-round pull-left">Change Password</a>
                            <button type="submit" name="submit" class="btn btn-warning pull-right">Edit Administrator</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-profile">
                    <div class="card-avatar">
                        <a href="#pablo">
                            <img style="width:120px;height:120px;overflow:hidden;" class="img" src="../uploads/<?php echo $user->image_path(); ?>" />
                        </a>
                    </div>
                    <div class="content">
                        <h6 class="category text-gray">Super Administrator</h6>
                        <h4 class="card-title">
                            <?php echo strtoupper($user->name); ?>
                        </h4>
                        <p class="card-content">
                            <div class="row">
                                <div class="col-sm-6">
                                    <b>Email</b>
                                </div>
                                <div class="col-sm-6">
                                    <?php echo $user->email; ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <b>Telephone</b>
                                </div>
                                <div class="col-sm-6">
                                    <?php echo $user->telephone; ?>
                                </div>
                            </div>
                        </p>
                        <a href="#" data-toggle="modal" data-target="#myModalProfile" class="btn btn-primary btn-round">Change Profile Photo</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModalPassword" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <form action="../process/edit_admin_password.php" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                    <legend>
                        Change Password
                    </legend>
                    <input type="hidden" name="identity" value="<?php echo $user->id;?>" id="identity" />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Old Password</label>
                                <input type="password" name="oldpassword" value="" class="form-control" required="required" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group label-floating">
                                <label class="control-label">New Password</label>
                                <input type="password" name="password" value="" class="form-control" required="required" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Confirm New Password</label>
                                <input type="password" name="confirmpassword" value="" class="form-control" required="required" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-block btn-success" type="submit" name="changepassword">
                            Change Password
                        </button>
                    </div>
                    <small class="text-danger">Please Sign Out and Login again To Enable Changes</small>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>
<div id="myModalProfile" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <form action="../process/edit_admin_photo.php" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                    <legend>CHANGE PROFILE PICTURE</legend>
                    <img class="img img-thumbnail img-responsive" style="max-width: 150px;" src="../uploads/<?php echo $user -> image_path(); ?>">
                    <div class="clearfix"></div>
                    <input type="hidden" name="identity" value="<?php echo $user->id;?>" id="identity" />
                    <div class="form-group">
                        <label for="file_upload" style="cursor:pointer;">Select New Profile Photo</label>
                        <input class="form-control" style="cursor:pointer;" type="file" name="file_upload" aria-describedby="sizing-addon2" required="required" />
                    </div>
                    <input type="hidden" name="previous_image" value="<?php echo $user->image_path(); ?>">
                    <div class="form-group">
                        <button class="btn btn-block btn-success" type="submit" name="change_photo">
                            CHANGE PROFILE PICTURE
                        </button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>

<?php endif; ?>