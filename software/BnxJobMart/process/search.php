<?php
    require_once '../includes/initialize.php';
    if(isset($_POST['submit'])){
        $role = $_POST['role'];
        $search = $_POST['search'];
        $search = strtolower($search);
        if (($role=='administrator' && strlen(strstr($search,'employee'))>0)||($role=='administrator' && strlen(strstr($search,'student'))>0)) {
            redirect_to('../profile/students');
        }elseif(($role=='administrator' && strlen(strstr($search,'compan'))>0)||($role=='administrator' && strlen(strstr($search,'job'))>0)){
            redirect_to('../profile/accounts');
        }elseif(($role=='administrator' && strlen(strstr($search,'contact'))>0)||($role=='administrator' && strlen(strstr($search,'message'))>0)||($role=='administrator' && strlen(strstr($search,'enquir'))>0)){
            redirect_to('../profile/enquiries');
        }elseif(($role=='administrator' && strlen(strstr($search,'appli'))>0)||($role=='administrator' && strlen(strstr($search,'add'))>0)){
            redirect_to('../profile/jobapplicants');
        }elseif(($role=='administrator' && strlen(strstr($search,'news'))>0)||($role=='administrator' && strlen(strstr($search,'sign'))>0)){
            redirect_to('../profile/newsletters');
        }elseif(($role=='student' && strlen(strstr($search,'special'))>0)||($role=='student' && strlen(strstr($search,'skill'))>0)){
            redirect_to('../profile/specialization');
        }elseif(($role=='student' && strlen(strstr($search,'credential'))>0)){
            redirect_to('../profile/credentials');
        }elseif(($role=='student' && strlen(strstr($search,'job'))>0)||($role=='student' && strlen(strstr($search,'ad'))>0)){
            redirect_to('../profile/adds');
        }elseif(($role=='student' && strlen(strstr($search,'app'))>0)){
            redirect_to('../profile/applications');
        }elseif(($role=='student' && strlen(strstr($search,'offer'))>0)){
            redirect_to('../profile/receivedoffers');
        }elseif(($role=='company' && strlen(strstr($search,'ad'))>0)||($role=='company' && strlen(strstr($search,'job'))>0)){
            redirect_to('../profile/jobs');
        }elseif(($role=='company' && strlen(strstr($search,'candidate'))>0)||($role=='company' && strlen(strstr($search,'employ'))>0)){
            redirect_to('../profile/applicants');
        }elseif(($role=='company' && strlen(strstr($search,'offer'))>0)){
            redirect_to('../profile/sentoffers');
        }else{
            $session->message("Unknown search");
            redirect_to('../profile');
        }
    }
?>