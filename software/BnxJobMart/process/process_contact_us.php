<?php
    require_once('../includes/initialize.php');
    if(isset($_POST['submit'])){
        $contactus = new ContactUs();
        $contactus->name = $_POST['name'];
        $contactus->email = $_POST['email'];
        $contactus->subject = $_POST['subject'];
        $contactus->message =$_POST['message'];
        $contactus->date = date('Y-m-d');
        $contactus->save();
        $session->message("Enquiry Sent Successfully");
        redirect_to("../public/");
    }
?>