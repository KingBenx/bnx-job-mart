<?php
    require_once '../includes/initialize.php';

    if(isset($_POST['change_file'])){
        $credentials = new Credentials();
        $credentials->id = $_POST['identity'];
        $credentials->attach_file($_FILES['file_upload']);
        $credentials->photo_update();
        $old_image_path="../uploads/".$_POST['previous_image'];
        unlink($old_image_path);
        $session->message("File updated successfully");
        redirect_to("../profile/credentials");
    }
	?>