<?php
    require_once '../includes/initialize.php';

    $student_profile=new Student();
    $student_profile->id=$_POST['identity'];
    $student_profile->password=password_hash($_POST['password'],PASSWORD_BCRYPT,array('cost'=>12));
    $oldpass = Student::find_by_id($_POST['identity']);

    if(!password_verify($_POST['confirmpassword'],$student_profile->password)){
        $session->message("Password fields don't match");
        redirect_to("../profile/studentprofile");
    } elseif(!password_verify($_POST['oldpassword'],$oldpass->password)){
        $session->message("Old password doesn't match");
        redirect_to("../profile/studentprofile");
    }else{
        $student_profile->update_password();
        $session->message("Password has been successfully updated");
        redirect_to("../profile/studentprofile");
    }

?>