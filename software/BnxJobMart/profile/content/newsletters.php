<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Newsletter Signups</h4>
                        <p class="category">Current newsletter signups in ascending order</p>
                        <strong class="text-info"><?php if(output_message($message)){ echo output_message($message); } ?></strong>  
                    </div>
                    <div class="card-content table-responsive table-bordered">
                        <table class="table">
                            <thead class="text-primary">
                                <th>Email</th>
                                <th>Reply</th>
                                <th>Delete</th>
                            </thead>
                            <tbody>
                                <?php
                                    $newsletter = Newsletter::find_all_order_by_field_asc('email');
                                    foreach ($newsletter as $value):
                                ?>
                                    <tr>
                                        <td>
                                            <?php echo $value->email; ?>
                                        </td>
                                        <td>
                                            <a href="mailto:<?php echo $value->email; ?>">
                                                <i class="material-icons">reply</i>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="../process/delete_newsletter.php?id=<?php echo $value->id; ?>">
                                                <i class="material-icons">delete_forever</i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>