<?php
    require_once '../includes/initialize.php';

    if(isset($_POST['change_photo'])){
        $getCompany = new Company();
        $getCompany->id = $_POST['identity'];
        $getCompany->attach_file($_FILES['file_upload']);
        $getCompany->photo_update();
        $old_image_path="../uploads/".$_POST['previous_image'];
        unlink($old_image_path);
        $session->message("Photo updated successfully");
        redirect_to("../profile/companyprofile");
    }
	?>