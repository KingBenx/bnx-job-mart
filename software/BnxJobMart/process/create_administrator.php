<?php
    require_once '../includes/initialize.php';

	if(isset($_POST['submit'])){
		$administrator = new Administrators();
		$administrator->id = $_POST['identity'];
		$administrator->name = $_POST['name'];
		$administrator->email = $_POST['email'];
		$administrator->role = "administrator";
		$administrator->telephone = $_POST['telephone'];
		$administrator->password = password_hash($_POST['password'],PASSWORD_BCRYPT,array('cost'=>12));
		$verifypassword = $_POST['verifypassword'];
		$administrator->access = $_POST['access'];
		$administrator->attach_file($_FILES['file_upload']);

		if(empty($administrator->name) || empty($administrator->email) || empty($administrator->role) || empty($administrator->telephone) || empty($administrator->access)){
			$session->message("Failed to add administrator. Empty fields or incorrect data type");
		} elseif(strcmp($_POST['password'],$verifypassword) !== 0){
				$session->message("Failed to add administrator. Password doesn't match");
		} elseif(isset($_POST['identity'])){
			$administrator->save();
			$session->message("Administrator $administrator->name Details Updated Successfully");
		} else {
			$administrator->save();
			$session->message("Administrator $administrator->name Saved successfully");
		}
		redirect_to("../profile/administrators");
	}
?>