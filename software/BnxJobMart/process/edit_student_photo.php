<?php
    require_once '../includes/initialize.php';

    if(isset($_POST['change_photo'])){
        $getStudent = new Student();
        $getStudent->id = $_POST['identity'];
        $getStudent->attach_file($_FILES['file_upload']);
        $getStudent->photo_update();
        $old_image_path="../uploads/".$_POST['previous_image'];
        unlink($old_image_path);
        $session->message("Photo updated successfully");
        redirect_to("../profile/studentprofile");
    }
	?>