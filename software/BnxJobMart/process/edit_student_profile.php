<?php
    require_once '../includes/initialize.php';

	if(isset($_POST['submit'])){
		$student_profile = new Student();
		$student_profile->id = $_POST['identity'];
		$student_profile->name = $_POST['name'];
        $student_profile->category = $_POST['category'];
        $student_profile->email = $_POST['email'];
        $student_profile->role = "student";
        $student_profile->telephone = $_POST['telephone'];
        $student_profile->student_no = $_POST['student_no'];
        $student_profile->access = 'grant';
        
		if(isset($_POST['identity'])){
			$student_profile->save();
			$session->message("Candidate $student_profile->name Details Updated Successfully");
		} else {
			$session->message("Candidate $student_profile->name Details unsuccessful");
		}
		redirect_to("../profile/studentprofile");
	}
?>