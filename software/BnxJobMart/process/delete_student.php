<?php
    require_once ('../includes/initialize.php');
	// if(!$session->is_logged_in()){ redirect_to("login"); }
?>
<?php
	if(empty($_GET['id'])){
		$session->message("No Student ID was provided. ");
		redirect_to('../profile/students');
	}

	$student = Student::find_by_id($_GET['id']);
	if($student->destroy()){
		$session->message("The Student {$student->name} was deleted.");
		redirect_to('../profile/students');
	} else {
		$session->message("The Student {$student->name} could not be deleted.");
		redirect_to('../profile/students');
	}
?>
<?php if(isset($database)){ $database->close_connection(); }