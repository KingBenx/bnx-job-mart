<?php
    require_once '../includes/initialize.php';

    if(isset($_POST['change_photo'])){
        $getAdministrator = new Administrators();
        $getAdministrator->id = $_POST['identity'];
        $getAdministrator->attach_file($_FILES['file_upload']);
        $getAdministrator->photo_update();
        $old_image_path="../uploads/".$_POST['previous_image'];
        unlink($old_image_path);
        $session->message("Photo updated successfully");
        redirect_to("../profile/administrators");
    }
	?>