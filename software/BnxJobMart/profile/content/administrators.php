<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Administrator Accounts</h4>
                        <p class="category">Current system administrators</p>
                        <strong class="text-info"><?php if(output_message($message)){ echo output_message($message); } ?></strong>  
                    </div>
                    <div class="card-content table-responsive table-bordered">
                        <table class="table">
                            <thead class="text-primary">
                                <th>Name</th>
                                <th>Contacts</th>
                                <th>Role</th>
                                <th>Access</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </thead>
                            <tbody>
                                <?php
                                    $administrator = Administrators::find_all_order_by_field_asc('access');
                                    foreach ($administrator as $value):
                                ?>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <img style="max-width: 120px;" class="img img-thumbnail img-responsive" src="../uploads/<?php echo $value->image_path(); ?>"
                                                    alt="">
                                            </div>
                                            <div class="row">
                                                <b>
                                                    <small>
                                                        <?php echo $value->name; ?>
                                                    </small>
                                                </b>
                                            </div>
                                        </td>
                                        <td>
                                            <?php echo $value->email; ?>
                                            <br/>
                                            <?php echo $value->telephone ?>
                                        </td>
                                        <td>
                                            <?php echo strtoupper($value->role); ?>
                                        </td>
                                        <td>
                                            <?php echo $value->access; ?>
                                        </td>
                                        <td>
                                            <a href="administrators/edit/<?php echo $value->id; ?>">
                                                <i class="material-icons">mode_edit</i>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="../process/delete_administrator.php?id=<?php echo $value->id; ?>">
                                                <i class="material-icons">delete_forever</i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <?php if(!isset($_GET['edit'])){ ?>
                <div class="card">
                    <div class="card-header" data-background-color="blue">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="title">Add Administrator</h4>
                                <p class="category">Update any information</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <form action="../process/create_administrator.php" enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Upload Profile</label>
                                        <input type="file" name="file_upload" value="" class="form-control" required="required"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Name</label>
                                        <input type="text" name="name" value="" class="form-control" required="required"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Email</label>
                                        <input type="email" name="email" value="" class="form-control" required="required"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Telephone</label>
                                        <input type="tel" name="telephone" value="" class="form-control" required="required"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Access</label>
                                        <select class="form-control" name="access" id="access" name="access" required="required">
                                            <option value="deny">deny</option>
                                            <option value="grant">grant</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Password</label>
                                        <input type="password" name="password" value="" class="form-control" required="required"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Verify Password</label>
                                        <input type="password" name="verifypassword" value="" class="form-control" required="required"/>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" name="submit" class="btn btn-primary pull-right">Add Administrator</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <?php }else{ $getAdministrator = Administrators::find_by_id($_GET['edit']);  ?>
                <div class="card">
                    <div class="card-header" data-background-color="orange">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4 class="title">Edit Administrator</h4>
                                <p class="category">Update any information</p>
                            </div>
                            <div class="col-sm-6">
                                <img class="img img-thumbnail img-responsive pull-right" style="max-width: 150px;" src="../uploads/<?php echo $getAdministrator->image_path();?>">
                                <div class="clearfix"></div>
                                <a class="btn btn-info pull-right" data-toggle="modal" data-target="#myModalAdministrator">Change Profile</a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <form action="../process/create_administrator.php" enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Name</label>
                                        <input type="text" name="name" value="<?php echo $getAdministrator->name; ?>" class="form-control" required="required"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Email</label>
                                        <input type="email" name="email" value="<?php echo $getAdministrator->email; ?>" class="form-control" required="required"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Telephone</label>
                                        <input type="tel" name="telephone" value="<?php echo $getAdministrator->telephone; ?>" class="form-control" required="required"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Access</label>
                                        <select class="form-control" name="access" id="access" name="access" required="required">
                                            <option value="<?php echo $getAdministrator->access; ?>">
                                                <?php echo $getAdministrator->access; ?>
                                            </option>
                                            <option value="deny">deny</option>
                                            <option value="grant">grant</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="identity" value="<?php echo $getAdministrator->id; ?>"/>
                            <button type="submit" name="submit" class="btn btn-warning pull-right">Edit Administrator</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <div id="myModalAdministrator" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                    &times;
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="../process/edit_administrator_photo.php" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                                    <legend>CHANGE PROFILE PICTURE</legend>
                                    <img class="img img-thumbnail img-responsive" style="max-width: 150px;" src="../uploads/<?php echo $getAdministrator -> image_path(); ?>"/>
                                    <div class="clearfix"></div>
                                    <input type="hidden" name="identity" value="<?php echo $getAdministrator->id;?>" id="identity" />
                                    <div class="form-group">
                                        <label for="file_upload">Photo</label>
                                        <input class="form-control" type="file" name="file_upload" aria-describedby="sizing-addon2" required="required" />
                                    </div>
                                    <input type="hidden" name="previous_image" value="<?php echo $getAdministrator->image_path(); ?>"/>
                                    <div class="form-group">
                                        <button class="btn btn-block btn-success" type="submit" name="change_photo">
                                            CHANGE PROFILE PHOTO
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>