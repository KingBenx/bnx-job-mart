<?php
// If it's going to need the database, then it's 
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class Offers extends DatabaseObject {
    
    protected static $table_name = "offer";
    protected static $db_fields = array('id','student_id','title','company_id','category','salary','description','offer_deadline','status');
    protected static $db_fields_insert = array('student_id','title','company_id','category','salary','description','offer_deadline','status');
    protected static $db_fields_update = array('title','category','salary','description','offer_deadline','status');
    
    public $id;
    public $company_id;
    public $student_id;
    public $title;
    public $category;
    public $salary;
    public $description;
    public $offer_deadline;
    public $status;
}
?>