<?php
// If it's going to need the database, then it's
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class Apply extends DatabaseObject {

	protected static $table_name = "job_applicants";
	protected static $db_fields = array('id', 'job_id','student_id','apply_date','interview_date');
	protected static $db_fields_insert = array('job_id','student_id','apply_date');
    protected static $db_fields_update = array('interview_date');
    
    public $id;
    public $job_id;
    public $student_id;
    public $apply_date;
    public $interview_date;
}
?>