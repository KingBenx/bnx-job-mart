<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div <?php if(!isset($_GET[ 'edit'])){ ?>class="col-md-12"
                <?php }else{?>class="col-md-8"
                <?php } ?>>
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Company Accounts</h4>
                        <p class="category">Current companies registered</p>
                        <strong class="text-info"><?php if(output_message($message)){ echo output_message($message); } ?></strong>  
                    </div>
                    <div class="card-content table-responsive table-bordered">
                        <table class="table">
                            <thead class="text-primary">
                                <th>Company</th>
                                <th>Description</th>
                                <th>Location</th>
                                <th>Contacts</th>
                                <th>Access</th>
                                <th>View Profile</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </thead>
                            <tbody>
                                <?php
                                    $company = Company::find_all_order_by_field_asc('access');
                                    foreach ($company as $value):
                                ?>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <img style="max-width: 120px;" class="img img-thumbnail img-responsive" src="../uploads/<?php echo $value->image_path(); ?>"
                                                    alt="">
                                            </div>
                                            <div class="row">
                                                <b>
                                                    <small>
                                                        <?php echo $value->name; ?>
                                                    </small>
                                                </b>
                                                <br/>
                                                <small>
                                                    <a href="<?php echo $value->url; ?>">
                                                        <?php echo $value->url; ?>
                                                    </a>
                                                </small>
                                            </div>

                                        </td>
                                        <td>
                                            <?php echo $value->company_description; ?>
                                        </td>
                                        <td>
                                            <?php echo $value->location; ?>
                                        </td>
                                        <td>
                                            <?php echo $value->telephone; ?><br/> 
                                            <?php echo $value->email; ?>
                                        </td>
                                        <td>
                                            <?php echo $value->access; ?>
                                        </td>
                                        <td>
                                        <a href="info/company/<?php echo $value->id; ?>">
                                            <i class="material-icons">info</i> View Info
                                        </a>
                                        </td>
                                        <td>
                                            <a href="accounts/edit/<?php echo $value->id; ?>">
                                                <i class="material-icons">mode_edit</i>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="../process/delete_company.php?id=<?php echo $value->id; ?>">
                                                <i class="material-icons">delete_forever</i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div <?php if(isset($_GET[ 'edit'])): ?>class="col-md-4"
                <?php endif; ?>>
                <?php  if(isset($_GET['edit'])){ $companydetails = Company::find_by_id($_GET['edit']); ?>
                <div class="card">
                    <div class="card-header" data-background-color="orange">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4 class="title">Edit Company</h4>
                                <p class="category">Update any information</p>
                            </div>
                            <div class="col-sm-6">
                                <img class="img img-thumbnail img-responsive pull-right" style="max-width: 150px;" src="../uploads/<?php echo $companydetails->image_path();?>">
                                <div class="clearfix"></div>
                                </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <form action="../process/process_company_superadmin.php" enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Company Name</label>
                                        <input type="text" name="name" value="<?php echo $companydetails->name; ?>" class="form-control" required="required"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Url Address</label>
                                        <input type="url" name="url" value="<?php echo $companydetails->url; ?>" class="form-control" required="required"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Location</label>
                                        <select class="form-control" name="location" id="location" name="location" required="required">
                                            <option value="inland">In Uganda</option>
                                            <option value="overseas">Outside Uganda</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Telephone</label>
                                        <input type="tel" name="telephone" value="<?php echo $companydetails->telephone; ?>" class="form-control" required="required"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Access</label>
                                        <select class="form-control" name="access" id="access" name="access" required="required">
                                            <option value="<?php echo $companydetails->access; ?>">
                                                <?php echo $companydetails->access; ?>
                                            </option>
                                            <option value="deny">deny</option>
                                            <option value="grant">grant</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>About Company</label>
                                        <div class="form-group label-floating">
                                            <label class="control-label"> Company Description.</label>
                                            <textarea class="form-control" name="description"  required="required">
                                                <?php echo $companydetails->company_description; ?>
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="identity" value="<?php echo $companydetails->id; ?>"/>
                            <button type="submit" name="submit" class="btn btn-warning pull-right">Update Company Profile</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <?php  } ?>
            </div>
        </div>
    </div>
</div>