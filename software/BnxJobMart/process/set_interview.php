<?php
    require_once('../includes/initialize.php');
    if(isset($_POST['submit'])){
        $apply = new Apply();
        $apply->id = $_POST['identity'];
        $apply->interview_date = $_POST['interview_date'];
        $apply->save();

        #get job details
        $applicationDetails = Apply::find_by_id($apply->id);
        $getJobDetails = Jobs::find_by_id($applicationDetails->job_id);
        $getEmployerDetails = Company::find_by_id($getJobDetails->company_id);
        
        #get student Details 
        $getStudentDetails = Student::find_by_id($applicationDetails->student_id);

        $message=wordwrap('Hello '.$getStudentDetails->name.' , '.$getEmployerDetails->name.' has set an interview date for you. Access our site for more information',70);
        $to=$getStudentDetails->email;
        $subject=$getJobDetails->title.' Application Feedback';
        $mail = new PHPMailer;
        $mail->isSendmail();
        $mail->setFrom('info@bnxjobmart.com', 'Bnx Job Mart');
        $mail->addReplyTo('info@bnxjobmart.com', 'Bnx Job Mart');
        $mail->addAddress($to, $getStudentDetails->name);
        $mail->Subject = $subject;
        $name = "Name: $getEmployerDetails->name<br><br>";
        $email = "Email: $getEmployerDetails->email<br><br>";
        $phone = "Phone: $getEmployerDetails->telephone<br><br>";
        $message = "Message: $message<br><br>";
        $referrer = $_SERVER['HTTP_REFERER'] ? '<br><br><br>This message was submitted from: ' . $_SERVER['HTTP_REFERER'] : '';
        $body = "$name $email $phone $message $referrer";
        $mail->msgHTML($body);
        $mail->AltBody = $body;
        $mail->addAttachment('../public/img/logo.png');
        if($mail->send())
            $session->message("Interview Set Successfully");
        redirect_to("../profile/applicants");
    }
?>