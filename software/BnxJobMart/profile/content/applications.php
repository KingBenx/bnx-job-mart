<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Application Status</h4>
                        <p class="category">Keep Track Of Your Job Applications</p>
                        <strong class="text-info">
                            <?php if(output_message($message)){ echo output_message($message); } ?>
                        </strong>
                    </div>
                    <div class="card-content table-responsive table-bordered">
                        <table class="table">
                            <thead class="text-primary">
                                <th>Job Title</th>
                                <th>Company</th>
                                <th>Description</th>
                                <th>Application Date</th>
                                <th>Interview Date</th>
                            </thead>
                            <tbody>
                                <?php
                                    $apply = Apply::find_by_sql('select*from job_applicants where student_id = '.$user->id.' order by id desc') ;
                                    foreach ($apply as $value):
                                ?>
                                    <tr>
                                        <td>
                                            <?php $getJob=Jobs::find_by_id($value->job_id); echo $getJob->title; ?>
                                        </td>
                                        <td>
                                        <div class="row">
                                                <img style="max-width: 120px;" class="img img-thumbnail img-responsive" src="../uploads/<?php  $getCompanyId=Jobs::find_by_id($value->job_id); $getCompany= Company::find_by_id($getCompanyId->company_id); echo $getCompany->image_path(); ?>"
                                                    alt="">
                                            </div>
                                            <div class="row">
                                                <b>
                                                    <small>
                                                        <?php  $getCompanyId=Jobs::find_by_id($value->job_id); $getCompany= Company::find_by_id($getCompanyId->company_id); echo $getCompany->name; ?>
                                                    </small>
                                                </b>
                                            </div>
                                        </td>
                                        <td>
                                        <?php  $getJob=Jobs::find_by_id($value->job_id); echo $getJob->description; ?>
                                        </td>
                                        <td>
                                            <?php $date=strtotime($value->apply_date); echo date('D M Y',$date); ?>
                                        </td>
                                        <td>
                                            <?php if($value->interview_date == NULL){
                                                echo "PENDING";
                                            }else{
                                                $date=strtotime($value->interview_date); echo date('D M Y',$date); 
                                            }
                                             ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>