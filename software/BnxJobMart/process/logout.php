<?php
	require_once '../includes/initialize.php';
	$_SESSION = array();
	if(isset($_COOKIE[$session->user_id])){
		setcookie($session->user_id, '', time() - 42000, '/');
        setcookie($session->user_role, '', time() - 42000, '/');
        setcookie($session->user_email, '', time() - 42000, '/');
	}
	$session->logout();
	session_destroy();
	$session->message("You have been signed out");
	redirect_to("../public");
?>