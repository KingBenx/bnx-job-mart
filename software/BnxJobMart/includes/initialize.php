<?php
	// error_reporting(E_ALL);
	//define the core paths
	//DIRECTORY_SEPARATOR is a php pre-defined constant
	//(\ for windows, and / for unix )
	defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);
	defined('SITE_ROOT') ? null :	define('SITE_ROOT', DS.'var'.DS.'www'.DS.'html'.DS.'www.bnxjobmart.com');
	defined('LIB_PATH') ? null : define('LIB_PATH', SITE_ROOT.DS.'includes');
	defined('BASE_PATH_PROFILE') ? null : define('BASE_PATH_PROFILE', 'http://127.0.0.1/www.bnxjobmart.com/profile/');
	defined('BASE_PATH_PUBLIC') ? null : define('BASE_PATH_PUBLIC','http://127.0.0.1/www.bnxjobmart.com/public/');
	//load config file first
	require_once(LIB_PATH.DS.'config.php');
	//load basic functions next so that everything after can use them
  	require_once(LIB_PATH.DS.'functions.php');
	//load core objects
	require_once(LIB_PATH.DS.'session.php');
	require_once(LIB_PATH.DS.'database.php');
	require_once(LIB_PATH.DS.'database_object.php');
	//load database-related classes
	require_once(LIB_PATH.DS.'administrators.php');
	require_once(LIB_PATH.DS.'newsletter.php');
	require_once(LIB_PATH.DS.'contact_us.php');
	require_once(LIB_PATH.DS.'company.php');
	require_once(LIB_PATH.DS.'student.php');
	require_once(LIB_PATH.DS.'specialization.php');
	require_once(LIB_PATH.DS.'credentials.php');
	require_once(LIB_PATH.DS.'jobs.php');
	require_once(LIB_PATH.DS.'offers.php');
	require_once(LIB_PATH.DS.'apply.php');
	//mail classes
	require_once(LIB_PATH.DS.'mail/class.phpmailer.php');
	require_once(LIB_PATH.DS.'mail/class.phpmaileroauth.php');
	require_once(LIB_PATH.DS.'mail/class.phpmaileroauthgoogle.php');
	require_once(LIB_PATH.DS.'mail/class.pop3.php');
	require_once(LIB_PATH.DS.'mail/class.smtp.php');
	require_once(LIB_PATH.DS.'mail/PHPMailerAutoload.php');
?>
