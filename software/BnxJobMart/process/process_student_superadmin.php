<?php
    require_once '../includes/initialize.php';

    if(isset($_POST['submit'])){
        $student = new Student();
        $student->id = $_POST['identity'];
        $student->name = $_POST['name'];
        $student->category = $_POST['category'];
        $student->email = $_POST['email'];
        $student->role = "student";
        $student->telephone = $_POST['telephone'];
        $student->student_no = $_POST['student_no'];
        $student->password = password_hash($_POST['password'],PASSWORD_BCRYPT,array('cost'=>12));
        $verifypassword = $_POST['verifypassword'];
        $student->access = $_POST['access'];
        $student->attach_file($_FILES['file_upload']);
         if(isset($_POST['identity'])){
            $student->save();
            $session->message("Student $student->name Registered successfully");
         }else{
            $session->message("Student $student->name failed to register");
         }
        redirect_to("../profile/students");
    }
?>