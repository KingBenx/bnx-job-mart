<?php
// If it's going to need the database, then it's 
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class Specialization extends DatabaseObject {
    
    protected static $table_name = "specialization";
    protected static $db_fields = array('id','student_id','specialization','level');
    protected static $db_fields_insert = array('student_id','specialization','level');
    protected static $db_fields_update = array('student_id','specialization','level');
    
    public $id;
    public $specialization;
    public $student_id;
    public $level;
}
?>