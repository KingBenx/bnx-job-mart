<?php
    require_once '../includes/initialize.php';

	if(isset($_POST['submit'])){
		$company_profile = new Company();
		$company_profile->id = $_POST['identity'];
		$company_profile->name = $_POST['name'];
        $company_profile->location = $_POST['location'];
        $company_profile->email = $_POST['email'];
        $company_profile->role = "company";
        $company_profile->telephone = $_POST['telephone'];
        $company_profile->url = $_POST['url'];
        $company_profile->access = 'grant';
        $company_profile->company_description = $_POST['company_description'];
        
		if(isset($_POST['identity'])){
			$company_profile->save();
			$session->message("Company $company_profile->name Details Updated Successfully");
		} else {
			$session->message("Company $company_profile->name Details unsuccessful");
		}
		redirect_to("../profile/companyprofile");
	}
?>