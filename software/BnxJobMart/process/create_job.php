<?php
    require_once '../includes/initialize.php';

	if(isset($_POST['submit'])){
		$job = new Jobs();
		$job->id = $_POST['identity'];
		$job->title = $_POST['title'];
		$job->company_id = $_POST['company_id'];
		$job->specialization_field = $_POST['specialization_field'];
		$job->vacancy_no = $_POST['vacancy_no'];
        $job->category = $_POST['category'];
        $job->salary = $_POST['salary'];
        $job->description = $_POST['description'];
        $job->application_deadline = $_POST['application_deadline'];

		if(empty($job->title)||empty($job->specialization_field)||empty($job->category)||empty($job->salary)||empty($job->application_deadline)||empty($job->vacancy_no)){
			$session->message("Failed to add Job. Empty fields or incorrect data type");
		} elseif(isset($_POST['identity'])){
			$job->save();
			$session->message("Job $job->title Details Updated Successfully");
		} else {
			$job->save();
			$session->message("Job $job->title Saved successfully");
		}
		redirect_to("../profile/jobs");
	}
?>