<?php
    require_once ('../includes/initialize.php');
    if(!$session->is_logged_in()){ redirect_to("../public"); }
?>
<?php
    if(empty($_GET['id'])){
        $session->message("No User ID was provided. ");
        redirect_to('../profile/newsletters');
    }

    $newsletter = Newsletter::find_by_id($_GET['id']);
    if($newsletter->delete()){
        $session->message("The Email {$newsletter->email} was deleted.");
        redirect_to('../profile/newsletters');
    } else {
        $session->message("The Email {$newsletter->email} was not deleted.");
        redirect_to('../profile/newsletters');
    }
?>
<?php if(isset($database)){ $database->close_connection(); }