<?php 
  require_once('../includes/initialize.php');
  if (isset($_POST['submit'])) {
    $email = trim($_POST['email']);
    $password = trim($_POST['password']);
    $find_administrator_by_email = Administrators::count_all_by_sql('email=\''.$email.'\' limit 1') ;
    $find_student_by_email = Student::count_all_by_sql('email=\''.$email.'\' limit 1') ;
    $find_company_by_email = Company::count_all_by_sql('email=\''.$email.'\' limit 1') ;
    
    if($find_administrator_by_email>=1){
      $found_user = Administrators::authenticate($email);
    }elseif($find_company_by_email>=1){
      $found_user = Company::authenticate($email);
    }elseif($find_student_by_email>=1){
      $found_user = Student::authenticate($email);
    }else{}
    if ($found_user->access=='grant') {
      if(password_verify($password,$found_user->password)){
        $session->login($found_user);
        log_action('Login', "{$found_user->name} logged in.");
        redirect_to("../profile/dashboard");
      }else{
        $message = "Email/password combination incorrect.";
      }
    } else {
      $message = "Email/password combination incorrect or access denied";
    }
  } else {
    if (isset($_GET['logout']) && $_GET['logout'] == 1) {
      $message = "You are now logged out!";
    }
    $email = "";
    $password = "";
  }
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Bnx Job Mart</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:url" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">

  <!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="">
  <meta name="twitter:title" content="">
  <meta name="twitter:description" content="">
  <meta name="twitter:image" content="">

  <!-- Favicon -->
  <link href="img/favicon.ico" rel="icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,700|Roboto:400,900" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">
</head>

<body>
  <!-- Page Content
    ================================================== -->
  <!-- Hero -->

  <section class="hero">
    <div class="container text-center">
      <div class="row">
        <div class="col-md-12">
          <a class="hero-brand" href="index.html" title="Home">
            <img alt="Bell Logo" src="img/logo.png">
          </a>
        </div>
      </div>

      <div class="col-md-12">
        <h1>
          Finding The Right Job or Finding The Right Employee
        </h1>

        <p class="tagline">
          We are about finding the right employee for the right Employer with a click of a finger.
        </p>
        <a class="btn btn-full" href="#about">Get Started Now</a>
      </div>
    </div>

  </section>
  <!-- /Hero -->

  <!-- Header -->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <a href="index.html">
          <img src="img/logo-nav.png" alt="" title="" /></img>
        </a>
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu" role="navigation">
          <li>
            <a href="#about">About Us</a>
          </li>
          <li>
            <a href="#features">Features</a>
          </li>
          <li>
            <a href="#contact">Contact Us</a>
          </li>
          <li>
            <a class="separator">|</a>
          </li>
          <li>
            <a href="#login" data-toggle="modal" data-target=".login">
              <i class="fa fa-lock"></i> Login</a>
          </li>
        </ul>
      </nav>
      <!-- #nav-menu-container -->

      <nav class="nav social-nav pull-right hidden-sm-down">
        <a href="#">
          <i class="fa fa-twitter"></i>
        </a>
        <a href="#">
          <i class="fa fa-facebook"></i>
        </a>
        <a href="#">
          <i class="fa fa-linkedin"></i>
        </a>
        <a href="#">
          <i class="fa fa-envelope"></i>
        </a>
      </nav>
    </div>
  </header>
  <!-- #header -->

  <!-- About -->

  <section class="about" id="about">
    <div class="container text-center">
      <h2>
        About Bnx Job Mart
      </h2>

      <p>
        Aptech Computer Education is a hightly specialized computer training well known for its supreme students in the Employment
        field. Here at Bnx Job Mart, we take the husstle out of the employment process by providing a portal for both Aptech
        students and the Employment Community.
        <br/>It's all about the right person for the right job
      </p>

      <div class="row stats-row">
        <div class="stats-col text-center col-md-3 col-sm-6">
          <div class="circle">
            <span class="stats-no" data-toggle="counter-up"><?php $employer_count = Company::count_all(); echo $employer_count; ?></span> Satisfied Employers
          </div>
        </div>

        <div class="stats-col text-center col-md-3 col-sm-6">
          <div class="circle">
            <span class="stats-no" data-toggle="counter-up"><?php $job_count = Jobs::count_all(); echo $job_count; ?></span> Available Jobs
          </div>
        </div>

        <div class="stats-col text-center col-md-3 col-sm-6">
          <div class="circle">
            <span class="stats-no" data-toggle="counter-up"><?php $student_count = Student::count_all(); echo $student_count; ?></span> Satisfied Students
          </div>
        </div>

        <div class="stats-col text-center col-md-3 col-sm-6">
          <div class="circle">
            <span class="stats-no" data-toggle="counter-up"><?php $student_count = Student::count_all(); echo $student_count; ?></span> Potential Employees
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /About -->
  <!-- Parallax -->

  <div class="block bg-primary block-pd-lg block-bg-overlay text-center" data-bg-img="img/parallax-bg.jpg" data-settings='{"stellar-background-ratio": 0.6}'
    data-toggle="parallax-bg">
    <h2>
      How to make the most of us
    </h2>
    <p>
      A person who feels appreciated will always do more than whats expected
    </p>
    <img alt="" class="gadgets-img hidden-md-down" src="img/community.png">
  </div>
  <!-- /Parallax -->
  <!-- Features -->

  <section class="features" id="features">
    <div class="container">
      <h2 class="text-center">
        Features
      </h2>

      <div class="row">
        <div class="feature-col col-lg-4 col-xs-12">
          <div class="card card-block text-center">
            <div>
              <div class="feature-icon">
                <span class="fa fa-globe"></span>
              </div>
            </div>

            <div>
              <h3>
                Latest Jobs &amp Projects
              </h3>

              <p>
                Find out the latest job offers and projects from companies around the globe.
              </p>
            </div>
          </div>
        </div>

        <div class="feature-col col-lg-4 col-xs-12">
          <div class="card card-block text-center">
            <div>
              <div class="feature-icon">
                <span class="fa fa-users"></span>
              </div>
            </div>

            <div>
              <h3>
                Multiple choices
              </h3>

              <p>
                Select a highly specialized work force from our vast pool of employees.
              </p>
            </div>
          </div>
        </div>

        <div class="feature-col col-lg-4 col-xs-12">
          <div class="card card-block text-center">
            <div>
              <div class="feature-icon">
                <span class="fa fa-handshake-o"></span>
              </div>
            </div>

            <div>
              <h3>
                Innovative Ideas &amp; Strong Bonds
              </h3>
              <p>
                Work with someone you trust.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /Features -->
  <!-- @component: footer -->

  <!-- Call to Action -->

  <section class="cta">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 col-sm-12 text-lg-left text-center">
          <h2>
            Sign up for newsletter
          </h2>

          <p>
            By signing up for our newsletters, you are provided by the latest information concerning our services
          </p>
        </div>

        <div class="col-lg-3 col-sm-12 text-lg-right text-center">
          <form action="../process/process_newsletter_signup.php" method="post">
            <div class="form-group row">
              <div class="col-sm-9">
                <input type="email" class="form-control" name="email" id="email" placeholder="Email" required="required">
              </div>
              <button type="submit" name="submit" class="col-sm-3 col-form-button">
                <i class="fa fa-paper-plane"></i>
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!-- /Call to Action -->
  <section id="contact">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <h2 class="section-title">Contact Us</h2>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-3 offset-lg-2">
          <div class="info">
            <div>
              <i class="fa fa-map-marker"></i>
              <p>1
                <sup>st</sup> Floor Conrad Plaza
                <br>Kampala, Uganda</p>
            </div>

            <div>
              <i class="fa fa-envelope"></i>
              <p>info@bnxjobmart.com</p>
            </div>

            <div>
              <i class="fa fa-phone"></i>
              <p>+256 788 123 456</p>
            </div>

          </div>
        </div>

        <div class="col-lg-5">
          <div class="form">
            <?php if(output_message($message)!=''): ?>
            <div class="sendmessage">Your message has been sent. Thank you!</div>
            <?php endif; ?>
            <form action="../process/process_contact_us.php" method="post" role="form" class="contactForm">
              <div class="form-group">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars"
                  required="required" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email"
                  required="required" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject"
                  required="required" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"
                  required="required"></textarea>
                <div class="validation"></div>
              </div>
              <div class="text-center">
                <button type="submit" name="submit">Send Message</button>
              </div>
            </form>
          </div>
        </div>

      </div>
    </div>
  </section>

  <footer class="site-footer">
    <div class="bottom">
      <div class="container">
        <div class="row">

          <div class="col-lg-6 col-xs-12 text-lg-left text-center">
            <p class="copyright-text">
              © Bnx Job Mart
            </p>
            <div class="credits">
              Developed by Asiimwe Benard
            </div>
          </div>

          <div class="col-lg-6 col-xs-12 text-lg-right text-center">
            <ul class="list-inline">
              <li class="list-inline-item">
                <a href="index.html">Home</a>
              </li>

              <li class="list-inline-item">
                <a href="#about">About Us</a>
              </li>

              <li class="list-inline-item">
                <a href="#features">Features</a>
              </li>

              <li class="list-inline-item">
                <a href="#contact">Contact</a>
              </li>
            </ul>
          </div>

        </div>
      </div>
    </div>
  </footer>
  <!-- login section-->
  <div class="modal fade login" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="panel_login-tab" data-toggle="tab" href="#panel_login" role="tab" aria-controls="panel_login"
              aria-expanded="true">Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="employer_signup-tab" data-toggle="tab" href="#employer_signup" role="tab" aria-controls="employer_signup">Employer Signup</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="student_signup-tab" data-toggle="tab" href="#student_signup" role="tab" aria-controls="student_signup">Student Signup</a>
          </li>
        </ul>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="panel_login" role="tabpanel" aria-labelledby="panel_login-tab">
            <div class="form">
              <form action="#" method="post" class="reqister-form">
                <?php if(output_message($message)!=''): ?>
                <div class="sendmessage">
                  <?php echo output_message($message); ?>
                </div>
                <?php endif; ?>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <input type="email" name="email" class="form-control" value="<?php echo htmlentities($email); ?>" id="exampleInputEmail1"
                    aria-describedby="emailHelp" placeholder="Enter email" required="required">
                  <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" name="password" class="form-control" value="<?php echo htmlentities($password); ?>" id="exampleInputPassword1"
                    placeholder="Password" required="required">
                </div>
                <button type="submit" name="submit" class="btn btn-primary">Login</button>
              </form>
            </div>
          </div>
          <div class="tab-pane fade" id="employer_signup" role="tabpanel" aria-labelledby="employer_signup-tab">
            <div class="form">
              <?php if(output_message($message)!=''): ?>
              <div class="sendmessage">
                <?php echo output_message($message); ?>
              </div>
              <?php endif; ?>
              <form class="reqister-form" action="../process/process_company.php" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                <legend>Employer Registration</legend>
                <div class="form-group">
                  <label class="custom-file">
                    <input type="file" id="file" name="file_upload" class="form-control custom-file-input" required="required">
                    <span class="custom-file-control"></span>
                    <small id="fileHelp" class="form-text text-muted">Upload Company Logo.</small>
                  </label>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="name" id="name" aria-describedby="textHelp" placeholder="Enter Company Name"
                    required="required">
                </div>
                <div class="form-group">
                  <input type="url" class="form-control" name="url" id="url" aria-describedby="urlHelp" placeholder="Enter Company Url" required="required">
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Enter Email" required="required">
                </div>
                <div class="form-group">
                  <input type="tel" class="form-control" name="telephone" id="telephone" aria-describedby="telHelp" placeholder="Enter Telephone"
                    required="required">
                </div>
                <div class="form-group text-left">
                  <label for="location">Choose location</label>
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" type="radio" name="location" id="inlineRadio1" value="inland"> In Uganda
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <label class="form-check-label">
                      <input class="form-check-input" type="radio" name="location" id="inlineRadio2" value="overseas"> Outside Uganda
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col">
                      <input type="password" id="password" name="password" class="form-control" placeholder="Password" required="required">
                    </div>
                    <div class="col">
                      <input type="password" id="confirmPassword" name="verifypassword" class="form-control" placeholder="Confirm Password" required="required">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <textarea name="description" id="description" name="description" class="textarea form-control" placeholder="Enter Brief description of Company"
                    required="required"></textarea>
                </div>
                <button type="submit" name="submit" class="btn btn-primary">Register Company</button>
              </form>
            </div>
          </div>
          <div class="tab-pane fade" id="student_signup" role="tabpanel" aria-labelledby="student_signup-tab">
            <div class="form">
              <form class="reqister-form" action="../process/process_student.php" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                <legend>Student Registration</legend>
                <div class="form-group">
                  <label class="custom-file">
                    <input type="file" id="file" name="file_upload" class="form-control custom-file-input" required="required">
                    <span class="custom-file-control"></span>
                    <small id="fileHelp" class="form-text text-muted">Upload Profile Picture.</small>
                  </label>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="name" id="name" aria-describedby="textHelp" placeholder="Enter Name" required="required">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="student_no" id="student_no" aria-describedby="textHelp" placeholder="Enter Student Number"
                    required="required">
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Enter Email" required="required">
                </div>
                <div class="form-group">
                  <input type="tel" class="form-control" name="telephone" id="telephone" aria-describedby="telHelp" placeholder="Enter Telephone"
                    required="required">
                </div>
                <div class="form-group text-justify">
                  <label class="mr-sm-2" for="inlineFormCustomSelect">Your Category</label>
                  <select class="custom-select mb-12 mr-sm-12 mb-sm-0" id="inlineFormCustomSelect" name="category" required="required">
                    <option selected>Choose Your Category</option>
                    <option value="it">Information Technology</option>
                    <option value="multimedia">Multimedia</option>
                    <option value="architecture">Architecture</option>
                  </select>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col">
                      <input type="password" id="password" name="password" class="form-control" placeholder="Password" required="required">
                    </div>
                    <div class="col">
                      <input type="password" id="confirmPassword" name="verifypassword" class="form-control" placeholder="Confirm Password" required="required">
                    </div>
                  </div>
                </div>
                <button type="submit" name="submit" class="btn btn-primary">Register Student</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end of login-->
  <a class="scrolltop" href="#">
    <span class="fa fa-angle-up"></span>
  </a>
  <!-- Required JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/tether/js/tether.min.js"></script>
  <script src="lib/stellar/stellar.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/easing/easing.js"></script>
  <script src="lib/stickyjs/sticky.js"></script>
  <script src="lib/parallax/parallax.js"></script>
  <script src="lib/lockfixed/lockfixed.min.js"></script>

  <!-- Template Specisifc Custom Javascript File -->
  <script src="js/custom.js"></script>
  <script src="js/panel.js"></script>

  <script src="contactform/contactform.js"></script>

</body>

</html>