<?php
    require_once '../includes/initialize.php';

    $company_profile=new Company();
    $company_profile->id=$_POST['identity'];
    $company_profile->password=password_hash($_POST['password'],PASSWORD_BCRYPT,array('cost'=>12));
    $oldpass = Company::find_by_id($_POST['identity']);

    if(!password_verify($_POST['confirmpassword'],$company_profile->password)){
        $session->message("Password fields don't match");
        redirect_to("../profile/companyprofile");
    } elseif(!password_verify($_POST['oldpassword'],$oldpass->password)){
        $session->message("Old password doesn't match");
        redirect_to("../profile/companyprofile");
    }else{
        $company_profile->update_password();
        $session->message("Password has been successfully updated");
        redirect_to("../profile/companyprofile");
    }

?>