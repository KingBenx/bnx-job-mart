<?php
// If it's going to need the database, then it's
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class Student extends DatabaseObject {

	protected static $table_name = "student";
	protected static $db_fields = array('id','name','student_no', 'email','telephone','role' , 'category', 'access', 'password','filename','type','size');
	protected static $db_fields_insert = array('name','student_no', 'email','role' ,'telephone', 'category', 'access', 'password','filename','type','size');
  	protected static $db_fields_update = array('name','student_no','role' , 'email','telephone', 'category', 'access');
	protected static $db_fields_password = array('password');
	protected static $db_fields_photo = array('filename','type','size');

	public $id;
	public $name;
	public $student_no;
	public $email;
	public $role;
	public $telephone;
  	public $category;
	public $access;
	public $filename;
	public $type;
	public $size;
	public $password;
	
	private $temp_path;
	protected $upload_dir = "StudentProfiles";
	public $errors = array();
	protected $upload_errors = array(
		UPLOAD_ERR_OK			=> "No errors",
		UPLOAD_ERR_INI_SIZE	=> "Larget than upload_max_filesize",
		UPLOAD_ERR_FORM_SIZE	=> "Larger than form MAX_FILE_SIZE",
		UPLOAD_ERR_PARTIAL		=> "Partial upload.",
		UPLOAD_ERR_NO_FILE		=> "No file.",
		UPLOAD_ERR_NO_TMP_DIR	=> "No temporary directory.",
		UPLOAD_ERR_CANT_WRITE	=> "Can't write to disk.",
		UPLOAD_ERR_EXTENSION	=> "File upload stopped by extension."
	);

	public function full_name() {
		if(isset($this->name)) {
			return $this->name;
		} else {
			return "";
		}
	}
	public static function authenticate($email=""){
		global $database;
		$email=$database->escape_value($email);
		$sql="select * from student where email='{$email}' limit 1";
		$result_array=self::find_by_sql($sql);
		return !empty($result_array) ? array_shift($result_array) : false;
	}

	// Pass in $_FILE(['uploadfile']) as an argument
	public function attach_file($file){
		//Perform error checking on the form parameters
		if(!$file || empty($file) || !is_array($file)){
			//error: nothing uploaded or wrong argument usage
			$this->errors[] = "No file was uploaded";
			return false;
		} elseif($file['error'] != 0) {
			//error: report ehat PHP says went wrong
			$this->errors[] = $this->upload_errors[$file['error']];
			return FALSE;
		} else {
			// set object attributes to the form parameters
			$this->temp_path = $file['tmp_name'];
			$this->filename  = basename($file['name']);
			$this->type      = $file['type'];
			$this->size	     = $file['size'];
			return TRUE;
			//no worries about saving anything to the database yet.
		}
	}

	public function destroy(){
		//First remove the database entry
		if($this->delete()){
			// then remove the file
			// note that even though the database object entry is gone, this oject
			// is still around (which lets me use $this->image_path())
			$target_path = SITE_ROOT.DS.'uploads'.DS.$this->image_path();
			return unlink($target_path) ? true : false;
		} else {
			// database delete failed
			return FALSE;
		}
	}

	public function photo_update(){
		//BEGIN PHOTO UPDATE
		//$this->destroy();
		if(!empty($this->errors)){return false;}

		if(empty($this->filename) || empty($this->temp_path)){
			$this->errors[] = "The file location was not available.";
			return false;
		}

		$target_path = SITE_ROOT.DS.'uploads'.DS.$this->upload_dir.DS.$this->filename;

		if(file_exists($target_path)){
			$this->errors[] = "The file {$this->filename} already exists.";
			return false;
		}

		if(move_uploaded_file($this->temp_path, $target_path)){
			if($this->update_photo()){
				unset($this->temp_path);
				return true;
			}
		} else {
			$this->errors[] = "The file upload failed, possibly due to incorrect permissions on the upload folder.";
			return false;
		}
		//END OF PHOTO UPDATE
	}
	public function save(){
		//A new record won't have an id yet
		if(isset($this->id)){
			$this->update();
		} else {
			//can't save if there are pre-existing errors
			if(!empty($this->errors)){return false;}
			//can't save without filename and temp location
			if(empty($this->filename) || empty($this->temp_path)){
				$this->errors[] = "The file location was not available.";
				return false;
			}
			//Determin the target_path
			$target_path = SITE_ROOT.DS.'uploads'.DS.$this->upload_dir.DS.$this->filename;

			//makse sure the file doesnt already exist
			if(file_exists($target_path)){
				$this->errors[] = "The file {$this->filename} already exists.";
				return false;
			}
			//Attempt to move file
			if(move_uploaded_file($this->temp_path, $target_path)){
				//Success
				//Save a corresponding entry to the database
				if($this->create()){
					//done with the temp_path, the file isn't there anymore
					unset($this->temp_path);
					return true;
				}
			} else {
				//File was not moved
				$this->errors[] = "The file upload failed, possibly due to incorrect permissions on the upload folder.";
				return false;
			}
		}
	}

	public function image_path(){
		return $this->upload_dir.DS.$this->filename;
	}
}
?>
