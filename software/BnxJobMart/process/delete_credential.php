<?php
    require_once ('../includes/initialize.php');
	// if(!$session->is_logged_in()){ redirect_to("login"); }
?>
<?php
	if(empty($_GET['id'])){
		$session->message("No Credential ID was provided. ");
		redirect_to('../profile/credentials');
	}

	$credential = Credentials::find_by_id($_GET['id']);
	if($credential->destroy()){
		$session->message("The Credential {$credential->category} was deleted.");
		redirect_to('../profile/credentials');
	} else {
		$session->message("The Credential {$credential->category} could not be deleted.");
		redirect_to('../profile/credentials');
	}
?>
<?php if(isset($database)){ $database->close_connection(); }