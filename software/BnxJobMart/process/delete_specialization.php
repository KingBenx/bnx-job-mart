<?php
    require_once ('../includes/initialize.php');
    if(!$session->is_logged_in()){ redirect_to("../public"); }
?>
<?php
    if(empty($_GET['id'])){
        $session->message("No Specialization ID was provided. ");
        redirect_to('../profile/specialization');
    }

    $specialization = Specialization::find_by_id($_GET['id']);
    if($specialization->delete()){
        $session->message("The Specialization {$specialization->specialization} was deleted.");
        redirect_to('../profile/specialization');
    } else {
        $session->message("The Specialization {$specialization->specialization} was not deleted.");
        redirect_to('../profile/specialization');
    }
?>
<?php if(isset($database)){ $database->close_connection(); }