<?php
    require_once '../includes/initialize.php';

    if(isset($_POST['change_photo'])){
        $admin_photo = new Administrators();
        $admin_photo->id = $_POST['identity'];
        $admin_photo->attach_file($_FILES['file_upload']);
        $admin_photo->photo_update();
        $old_image_path="../uploads/".$_POST['previous_image'];
        unlink($old_image_path);
        $session->message("Profile Photo updated successfully");
        redirect_to("../profile/adminprofile");
    }
	?>