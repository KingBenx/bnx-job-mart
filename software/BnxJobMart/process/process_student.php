<?php
    require_once '../includes/initialize.php';

    if(isset($_POST['submit'])){
        $student = new Student();
        $student->name = $_POST['name'];
        $student->category = $_POST['category'];
        $student->email = $_POST['email'];
        $student->role = "student";
        $student->telephone = $_POST['telephone'];
        $student->student_no = $_POST['student_no'];
        $student->password = password_hash($_POST['password'],PASSWORD_BCRYPT,array('cost'=>12));
        $verifypassword = $_POST['verifypassword'];
        $student->access = 'deny';
        $student->attach_file($_FILES['file_upload']);
        if(strcmp($_POST['password'],$verifypassword) !== 0){
                $session->message("Failed to add Student. Password doesn't match");
        }else {
            $student->save();
            $session->message("Student $student->name Registered successfully");
        }
        redirect_to("../public/");
    }
?>