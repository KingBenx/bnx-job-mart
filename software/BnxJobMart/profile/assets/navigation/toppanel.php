<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"> Welcome
                <?php echo $user->name; ?> </a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="<?php if($user->role=='administrator'){ echo 'adminprofile';}elseif($user->role=='student'){ echo 'studentprofile';}else{ echo 'companyprofile'; } ?>">
                        <i class="material-icons">settings</i>
                    </a>
                </li>
                <li>
                    <a href="../process/logout.php">
                        <i class="material-icons">settings_power</i>
                    </a>
                </li>
            </ul>
            <form action="../process/search.php" method="post" class="navbar-form navbar-right" role="search">
                <div class="form-group  is-empty">
                    <input type="text" name="search" class="form-control" placeholder="Search">
                    <span class="material-input"></span>
                </div>
                <input type="hidden" name="role" value="<?php echo $user->role; ?>" />
                <button name="submit" type="submit" class="btn btn-white btn-round btn-just-icon">
                    <i class="material-icons">search</i>
                    <div class="ripple-container"></div>
                </button>
            </form>
        </div>
    </div>
</nav>