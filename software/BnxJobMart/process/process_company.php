<?php
    require_once '../includes/initialize.php';

    if(isset($_POST['submit'])){
        $company = new Company();
        $company->name = $_POST['name'];
        $company->url = $_POST['url'];
        $company->email = $_POST['email'];
        $company->role = "company";
        $company->telephone = $_POST['telephone'];
        $company->location = $_POST['location'];
        $company->password = password_hash($_POST['password'],PASSWORD_BCRYPT,array('cost'=>12));
        $verifypassword = $_POST['verifypassword'];
        $company->access = 'grant';
        $company->company_description = $_POST['description'];
        $company->attach_file($_FILES['file_upload']);
        if(strcmp($_POST['password'],$verifypassword) !== 0){
                $session->message("Failed to add Company. Password doesn't match");
        }else {
            $company->save();
            $session->message("Company $company->name Registered successfully");
        }
        redirect_to("../public/");
    }
?>