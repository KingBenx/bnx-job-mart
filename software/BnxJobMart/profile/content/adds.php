<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Company Jobs</h4>
                        <p class="category">Currently advertised Jobs</p>
                        <strong class="text-info">
                            <?php if(output_message($message)){ echo output_message($message); } ?>
                        </strong>
                    </div>
                    <div class="card-content table-responsive table-bordered">
                        <table class="table">
                            <thead class="text-primary">
                                <th>Title</th>
                                <th>Company</th>
                                <th>Info</th>
                                <th>Description</th>
                                <th>Application Deadline</th>
                                <th>Apply</th>
                            </thead>
                            <tbody>
                                <?php
                                    $job = Jobs::find_by_sql('select*from job where specialization_field = \''.$user->category.'\' order by id desc') ;
                                    foreach ($job as $value):
                                ?>
                                    <tr>
                                        <td>
                                            <?php echo strtoupper($value->title); ?>
                                        </td>
                                        <td>
                                        <div class="row">
                                                <img style="max-width: 120px;" class="img img-thumbnail img-responsive" src="../uploads/<?php $getCompany=Company::find_by_id($value->company_id); echo $getCompany->image_path(); ?>"
                                                    alt="">
                                            </div>
                                            <div class="row">
                                                <b>
                                                    <small>
                                                        <?php  $getCompany=Company::find_by_id($value->company_id); echo $getCompany->name; ?>
                                                    </small>
                                                </b>
                                            </div>
                                        </td>
                                        <td>
                                            <p>
                                                <b>Specialization : </b>
                                                <?php echo strtoupper($value->specialization_field); ?>
                                                <br/>
                                                <b>Vacancies : </b>
                                                <?php echo $value->vacancy_no; ?>
                                                <br/>
                                                <b>Category : </b>
                                                <?php echo $value->category; ?>
                                                <br/>
                                                <b>Salary : </b>
                                                <?php echo strtoupper($value->salary); ?>
                                            </p>
                                        </td>
                                        <td>
                                            <?php echo $value->description; ?>
                                        </td>
                                        <td>
                                            <?php $date=strtotime($value->application_deadline); echo date('D M Y',$date); ?>
                                        </td>
                                        <td>
                                            <a href="../process/apply.php?job=<?php echo $value->id; ?>&applicant=<?php echo $user->id; ?>">
                                                <i class="material-icons">thumb_up</i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>