<?php
    require_once ('../includes/initialize.php');
    if(!$session->is_logged_in()){ redirect_to("../public"); }
?>
<?php
    if(empty($_GET['id'])){
        $session->message("No Offer ID was provided. ");
        redirect_to('../profile/receivedoffers');
    }

    $offer = Offers::find_by_id($_GET['id']);
    if($offer->delete()){
        $session->message("The Offer {$offer->title} was deleted.");
        redirect_to('../profile/receivedoffers');
    } else {
        $session->message("The Offer {$offer->title} was not deleted.");
        redirect_to('../profile/receivedoffers');
    }
?>
<?php if(isset($database)){ $database->close_connection(); }