<?php require_once('../includes/initialize.php'); 
	  if(!$session->is_logged_in()||!isset($session->user_id)){ redirect_to("../public"); }
      $adminInstance = new Administrators();
      $studentInstance = new Student();
      $companyInstance = new Company();

      $userID = (int)$session->user_id;
      $userEmail = $_SESSION['user_email'];
    
      $findAdministrator = Administrators::count_all_by_sql('id = '.$userID.' and email = \''.$userEmail.'\' limit 1');
      $findCompany = Company::count_all_by_sql('id = '.$userID.' and email = \''.$userEmail.'\' limit 1');
      $findStudent = Student::count_all_by_sql('id = '.$userID.' and email = \''.$userEmail.'\' limit 1');
    
      if($findAdministrator==1){
        $user = $adminInstance->find_by_id($userID);
      }elseif($findStudent==1){
        $user = $studentInstance->find_by_id($userID);
      }elseif($findCompany==1){
        $user = $companyInstance->find_by_id($userID);
      }else{
        redirect_to("../public");
      }
?>
<!doctype html>
<html lang="en">
<base href="<?php echo BASE_PATH_PROFILE; ?>" target="_self">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Bnx Job Mart</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-1.jpg">
            <div class="logo">
                <a class="simple-text">
                    <img style="max-width:120px;" class="img img-responsive img-thumbnail" src="../uploads/<?php echo $user->image_path(); ?>"
                        alt="">

                </a>
            </div>
            <!--side panel -->
            <?php require_once('assets/navigation/sidepanel.php'); ?>
            <!--end side panel -->
        </div>
        <div class="main-panel">
            <!-- top panel -->
            <?php require_once('assets/navigation/toppanel.php'); ?>
            <!-- end top panel -->
            <!-- content -->
            <?php
                if(isset($_GET['page'])){
                    $page = $_GET['page'];
                    switch($page){
                        case 'adminprofile':
                            require_once('content/administrator_profile.php');
                            break;
                        case 'studentprofile':
                            require_once('content/student_profile.php');
                            break;
                        case 'companyprofile':
                            require_once('content/company_profile.php');
                            break;
                        case 'accounts':
                            require_once('content/accounts.php');
                            break;
                        case 'students':
                            require_once('content/students.php');
                            break;
                        case 'enquiries':
                            require_once('content/enquiries.php');
                            break;
                        case 'newsletters':
                            require_once('content/newsletters.php');
                            break;
                        case 'administrators':
                            require_once('content/administrators.php');
                            break;
                        case 'specialization':
                            require_once('content/specialization.php');
                            break;
                        case 'credentials':
                            require_once('content/credentials.php');
                            break;
                        case 'jobs':
                            require_once('content/jobs.php');
                            break;
                        case 'candidates':
                            require_once('content/candidates.php');
                            break;
                        case 'sentoffers':
                            require_once('content/sent_offers.php');
                            break;
                        case 'receivedoffers':
                            require_once('content/received_offers.php');
                            break;
                        case 'adds':
                            require_once('content/adds.php');
                            break;
                        case 'applications':
                            require_once('content/applications.php');
                            break;
                        case 'applicants':
                            require_once('content/applicants.php');
                            break;
                        case 'jobapplicants':
                            require_once('content/job_applicants.php');
                            break;
                        default:
                            require_once('content/dashboard.php');
                            break;
                    }
                }elseif(isset($_GET['info'])){
                    if($_GET['info']=='student'){
                        require_once('content/studentbio.php');
                    }elseif($_GET['info']=='company'){
                        require_once('content/companybio.php');
                    }
                }else{
                    require_once('content/dashboard.php');
                }
            ?>
                <!-- end content here -->
                <!--footer -->
                <?php require_once('assets/navigation/footer.php') ?>
                <!-- end footer -->
        </div>
    </div>
</body>
<!-- scripts -->
<?php require_once('assets/resources/scripts.php'); ?>
<!-- scripts end -->

</html>